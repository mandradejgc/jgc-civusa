<?php
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function progression_page_metaboxes( array $meta_boxes_pro ) {
	
	$page_fields_pro = array(
		array( 
			'id' => 'progression_slider',  
			'name' => __('Revolution Slider: Copy/Paste Slider Shortcode', 'progression'),
			'type' => 'text' 
		)
	);

	$meta_boxes_pro[] = array(
		'title' => __('Page Settings', 'progression'),
		'pages'      => array('page'),
		'priority'   => 'high', // Meta box priority
		'context'    => 'normal', // Meta box context
		'fields' => $page_fields_pro
	);

	return $meta_boxes_pro;

}
add_filter( 'cmb_meta_boxes', 'progression_page_metaboxes' );



function progression_post_metaboxes( array $meta_boxes_pro ) {
	
	$post_fields_pro = array(
		array( 
			'id' => 'pro_gallery',  
			'name' => __('Image Gallery', 'progression'),
			'desc' => __('Add-in images to display a gallery.', 'progression'),
			'type' => 'image',
			'repeatable' => true
		),
		array( 
			'id' => 'pro_video_post',  
			'name' => __('Audio/Video Embed', 'progression'),
			'desc' => __('Paste in your video url or embed code', 'progression'),
			'type' => 'text'
		),
		array( 
			'id' => 'pro_external_link',  
			'name' => __('External Link', 'progression'),
			'desc' => __('Make your post link to another page', 'progression'),
			'type' => 'text'
		)
	);

	$meta_boxes_pro[] = array(
		'title' => __('Post Settings', 'progression'),
		'pages'      => array('post'),
		'priority'   => 'high', // Meta box priority
		'context'    => 'normal', // Meta box context
		'fields' => $post_fields_pro
	);

	return $meta_boxes_pro;

}
add_filter( 'cmb_meta_boxes', 'progression_post_metaboxes' );




function progression_event_metaboxes( array $meta_boxes_pro ) {
	
	$post_fields_pro = array(
		array( 
			'id' => 'pro_gallery',  
			'name' => __('Image Gallery', 'progression'),
			'desc' => __('Add-in images to display a gallery.', 'progression'),
			'type' => 'image',
			'repeatable' => true
		),
		array( 
			'id' => 'pro_video_post',  
			'name' => __('Audio/Video Embed', 'progression'),
			'desc' => __('Paste in your video url or embed code', 'progression'),
			'type' => 'text'
		),
		array( 
			'id' => 'pro_external_link',  
			'name' => __('External Link', 'progression'),
			'desc' => __('Make your post link to another page', 'progression'),
			'type' => 'text'
		)
	);

	$meta_boxes_pro[] = array(
		'title' => __('Event Settings', 'progression'),
		'pages'      => array('event'),
		'priority'   => 'high', // Meta box priority
		'context'    => 'normal', // Meta box context
		'fields' => $post_fields_pro
	);

	return $meta_boxes_pro;

}
add_filter( 'cmb_meta_boxes', 'progression_event_metaboxes' );




function progression_wine_metaboxes( array $meta_boxes_pro ) {
	
	$post_fields_pro = array(
		array( 
			'id' => 'pro_wine_sub_header',  
			'name' => __('Wine Header', 'progression'),
			'desc' => __('Add-in a sub-header for your post. Example - Year: 2011, Alcohol: 14.5%', 'progression'),
			'type' => 'text'
		),
		array( 
			'id' => 'pro_wine_price',  
			'name' => __('Wine Price', 'progression'),
			'desc' => __('Add-in a Price for your Wine post. Example - $47', 'progression'),
			'type' => 'text'
		),		
		array( 
			'id' => 'pro_gallery',  
			'name' => __('Image Gallery', 'progression'),
			'desc' => __('Add-in images to display a gallery.', 'progression'),
			'type' => 'image',
			'repeatable' => true
		),
		array( 
			'id' => 'pro_video_post',  
			'name' => __('Audio/Video Embed', 'progression'),
			'desc' => __('Paste in your video url or embed code', 'progression'),
			'type' => 'text'
		),
		array( 
			'id' => 'pro_external_link',  
			'name' => __('External Link', 'progression'),
			'desc' => __('Make your post link to another page', 'progression'),
			'type' => 'text'
		)
	);

	$meta_boxes_pro[] = array(
		'title' => __('Wine Settings', 'progression'),
		'pages'      => array('wine'),
		'priority'   => 'high', // Meta box priority
		'context'    => 'normal', // Meta box context
		'fields' => $post_fields_pro
	);

	return $meta_boxes_pro;

}
add_filter( 'cmb_meta_boxes', 'progression_wine_metaboxes' );