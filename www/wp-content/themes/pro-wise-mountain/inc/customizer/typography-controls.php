<?php
/**
 * progression Theme Customizer
 *
 * @package progression
 */

function pro_add_tab_to_panel( $tabs ) {

    // Do this for each tab you want to create.
    // Make sure the array index matches the
    // 'name' array property.
    $tabs['pro-global'] = array(
        'name'        => 'pro-global',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Global', 'progression'),
        'description' => '',
        'sections'    => array(),
    );
	
	
	
	//Default Headings
    $tabs['pro-default-headings'] = array(
        'name'        => 'pro-default-headings',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Default Headings', 'progression'),
        'description' => '',
        'sections'    => array(),
    );
	
	
	//Navigation
    $tabs['pro-nav-headings'] = array(
        'name'        => 'pro-nav-headings',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Navigation', 'progression'),
        'description' => '',
        'sections'    => array(),
    );
	
	//Slider Revolution
    $tabs['pro-slider-headings'] = array(
        'name'        => 'pro-slider-headings',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Homepage Slider', 'progression'),
        'description' => '',
        'sections'    => array(),
    );
	
	
	//Sidebar
    $tabs['pro-sidebar-fonts'] = array(
        'name'        => 'pro-sidebar-fonts',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Sidebar', 'progression'),
        'description' => '',
        'sections'    => array(),
    );
	
	
	//Footer
    $tabs['pro-footer-fonts'] = array(
        'name'        => 'pro-footer-fonts',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Footer', 'progression'),
        'description' => '',
        'sections'    => array(),
    );
	
	
	//Wine Styles
    $tabs['pro-wine-fonts'] = array(
        'name'        => 'pro-wine-fonts',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Wines', 'progression'),
        'description' => '',
        'sections'    => array(),
    );

	//Event Styles
    $tabs['pro-event-fonts'] = array(
        'name'        => 'pro-event-fonts',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Events', 'progression'),
        'description' => '',
        'sections'    => array(),
    );
	
	//Buttons
    $tabs['pro-button-fonts'] = array(
        'name'        => 'pro-button-fonts',
        'panel'       => 'typography_panel_pro',
        'title'       => __('Buttons', 'progression'),
        'description' => '',
        'sections'    => array(),
    );
	
	
	
	
    // Return the tabs.
    return $tabs;
}
add_filter( 'tt_font_get_settings_page_tabs', 'pro_add_tab_to_panel' );

/**
 * How to add a font control to your tab
 *
 * @see  parse_font_control_array() - in class EGF_Register_Options
 *       in includes/class-egf-register-options.php to see the full
 *       properties you can add for each font control.
 *
 *
 * @param   array $controls - Existing Controls.
 * @return  array $controls - Controls with controls added/removed.
 *
 * @since 1.0
 * @version 1.0
 *
 */
function pro_add_control_to_tab( $controls ) {

    /**
     * 1. Removing default styles because we add-in our own
     */
    unset( $controls['tt_default_body'] );
    unset( $controls['tt_default_heading_1'] );
    unset( $controls['tt_default_heading_2'] );
    unset( $controls['tt_default_heading_3'] );
    unset( $controls['tt_default_heading_4'] );
    unset( $controls['tt_default_heading_5'] );
    unset( $controls['tt_default_heading_6'] );

    /**
     * 2. Now custom examples that are theme specific
     */

	/* Body Font */
    $controls['pro_body_font'] = array(
        'name'       => 'pro_body_font',
		'type'        => 'font',
        'title'      =>  __('Body Font', 'progression'),
        'tab'        => 'pro-global',
        'properties' => array( 'selector'   => 'body' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'crimson_text',
				'font_name'                  => 'Crimson Text',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#888888',
				'line_height'                => '1.6',
				'text_decoration'            => 'none',
				'text_transform'             => 'none',
				'font_size'                  => array( 'amount' => '16', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '0', 'unit' => 'px' )
			),
    );
    $controls['pro_link_main'] = array(
        'name'       => 'pro_link_main',
		'type'        => 'font',
        'title'      =>  __('Default Link ', 'progression'),
        'tab'        => 'pro-global',
        'properties' => array( 'selector'   => 'a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#4abd92',
			),
    );
    $controls['pro_link_main_hover'] = array(
        'name'       => 'pro_link_main_hover',
		'type'        => 'font',
        'title'      =>  __('Default Link Hover', 'progression'),
        'tab'        => 'pro-global',
        'properties' => array( 'selector'   => 'a:hover, .comment-navigation a:hover, h2.menu-title-pro a:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#696969',
			),
    );
	
    $controls['pro_highlight_global'] = array(
        'name'       => 'pro_highlight_global',
		'type'        => 'font',
        'title'      =>  __('Highlight Widget Text ', 'progression'),
        'tab'        => 'pro-global',
        'properties' => array( 'selector'   => '.text-pro-widget' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'crimson_text',
				'font_name'                  => 'Crimson Text',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
				'line_height'                => '1.3',
				'text_decoration'            => 'none',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '52', 'unit' => 'px' ),
				'padding_top'             => array( 'amount' => '80', 'unit' => 'px' ),
				'padding_bottom'             => array( 'amount' => '80', 'unit' => 'px' )
			),
    );	
	
	
	
	
	
	
	
	
	
	
	/* Headings */
    $controls['pro_page_title_font'] = array(
        'name'       => 'pro_page_title_font',
		'type'        => 'font',
        'title'      =>  __('Page Title', 'progression'),
        'tab'        => 'pro-default-headings',
        'properties' => array( 'selector'   => '#page-header-pro h1' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '300',
				'font_color'                 => '#ffffff',
				'line_height'                => '1.2',
				'text_decoration'            => 'none',
				'text_transform'             => 'none',
				'font_size'                  => array( 'amount' => '57', 'unit' => 'px' ),
				'padding_bottom'             => array( 'amount' => '50', 'unit' => 'px' ),
				'padding_top'          		 => array( 'amount' => '175', 'unit' => 'px' )
			),
    );
    $controls['pro_heading_1'] = array(
        'name'       => 'pro_heading_1',
		'type'        => 'font',
        'title'      =>  __('Heading 1', 'progression'),
        'tab'        => 'pro-default-headings',
        'properties' => array( 'selector'   => 'h1' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '300',
				'font_color'                 => '#4abd92',
				'line_height'                => '1.2',
				'text_decoration'            => 'none',
				'text_transform'             => 'none',
				'font_size'                  => array( 'amount' => '40', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '0', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '25', 'unit' => 'px' )
			),
    );
    $controls['pro_heading_2'] = array(
        'name'       => 'pro_heading_2',
		'type'        => 'font',
        'title'      =>  __('Heading 2', 'progression'),
        'tab'        => 'pro-default-headings',
        'properties' => array( 'selector'   => 'h2' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '300',
				'font_color'                 => '#4abd92',
				'line_height'                => '1.4',
				'text_decoration'            => 'none',
				'text_transform'             => 'none',
				'font_size'                  => array( 'amount' => '36', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '0', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '25', 'unit' => 'px' )
			),
    );
    $controls['pro_heading_3'] = array(
        'name'       => 'pro_heading_3',
		'type'        => 'font',
        'title'      =>  __('Heading 3', 'progression'),
        'tab'        => 'pro-default-headings',
        'properties' => array( 'selector'   => 'h3' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '300',
				'font_color'                 => '#4abd92',
				'line_height'                => '1.4',
				'text_decoration'            => 'none',
				'text_transform'             => 'none',
				'font_size'                  => array( 'amount' => '28', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '0', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '25', 'unit' => 'px' )
			),
    );
    $controls['pro_heading_4'] = array(
        'name'       => 'pro_heading_4',
		'type'        => 'font',
        'title'      =>  __('Heading 4', 'progression'),
        'tab'        => 'pro-default-headings',
        'properties' => array( 'selector'   => 'h4' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '300',
				'font_color'                 => '#4abd92',
				'line_height'                => '1.4',
				'text_decoration'            => 'none',
				'text_transform'             => 'none',
				'font_size'                  => array( 'amount' => '26', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '0', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '18', 'unit' => 'px' )
			),
    );
    $controls['pro_heading_5'] = array(
        'name'       => 'pro_heading_5',
		'type'        => 'font',
        'title'      =>  __('Heading 5', 'progression'),
        'tab'        => 'pro-default-headings',
        'properties' => array( 'selector'   => 'h5' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#4abd92',
				'line_height'                => '1.4',
				'text_decoration'            => 'none',
				'text_transform'             => 'none',
				'font_size'                  => array( 'amount' => '24', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '0', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '18', 'unit' => 'px' )
			),
    );
    $controls['pro_heading_66'] = array(
        'name'       => 'pro_heading_66',
		'type'        => 'font',
        'title'      =>  __('Heading 6', 'progression'),
        'tab'        => 'pro-default-headings',
        'properties' => array( 'selector'   => 'h6' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#4abd92',
				'line_height'                => '1.4',
				'text_decoration'            => 'none',
				'text_transform'             => 'none',
				'font_size'                  => array( 'amount' => '17', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '0', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '18', 'unit' => 'px' )
			),
    );
    $controls['pro_heading_store'] = array(
        'name'       => 'pro_heading_store',
		'type'        => 'font',
        'title'      =>  __('Store Heading', 'progression'),
        'tab'        => 'pro-default-headings',
        'properties' => array( 'selector'   => '#content-pro ul li.product h3' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '700',
				'font_color'                 => '#505045',
				'line_height'                => '1.4',
				'text_decoration'            => 'none',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '16', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '1.5', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '0', 'unit' => 'px' )
			),
    );	
	
	
	
	
	// Navigation Headings
    $controls['pro_nav_font_color'] = array(
        'name'       => 'pro_nav_font_color',
		'type'        => 'font',
        'title'      =>  __('Navigation Default', 'progression'),
        'tab'        => 'pro-nav-headings',
        'properties' => array( 'selector'   => '.sf-menu a, ul.mobile-menu-pro a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
				'line_height'                => '1',
				'text_decoration'            => 'none',
				'text_transform'             => 'capitalize',
				'letter_spacing'             => array( 'amount' => '1', 'unit' => 'px' ),
				'font_size'                  => array( 'amount' => '14', 'unit' => 'px' ),
				'padding_top'                  => array( 'amount' => '50', 'unit' => 'px' ),
				'padding_right'                  => array( 'amount' => '22', 'unit' => 'px' ),
				'padding_bottom'                  => array( 'amount' => '25', 'unit' => 'px' ),
				'padding_left'                  => array( 'amount' => '22', 'unit' => 'px' ),
			),
    );
	
	
    $controls['newpro_nav_hover_font_color'] = array(
        'name'       => 'newpro_nav_hover_font_color',
		'type'        => 'font',
        'title'      =>  __('Navigation Selected/Hover', 'progression'),
        'tab'        => 'pro-nav-headings',
        'properties' => array( 'selector'   => '.sf-menu li.current-menu-item a, .sf-menu a:hover, .sf-menu li.sfHover a, .menu-show-hide-pro, ul.mobile-menu-pro a:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#4abd92',
				'text_decoration'            => 'none',
			),
    );
	
    $controls['newpro_sub_nav_font'] = array(
        'name'       => 'newpro_sub_nav_font',
		'type'        => 'font',
        'title'      =>  __('Sub-Navigation Default', 'progression'),
        'tab'        => 'pro-nav-headings',
        'properties' => array( 'selector'   => '.sf-menu li li a, .sf-menu li.sfHover li a, .sf-menu li.sfHover li.sfHover li a, .sf-menu li.sfHover li.sfHover li.sfHover li a, .sf-menu li.sfHover li.sfHover li.sfHover li.sfHover li a, .sf-menu li.sfHover li.sfHover li.sfHover li.sfHover li.sfHover li a, ul.mobile-menu-pro li ul a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_weight_style'                => '300',
				'font_color'                 => '#a8a8a8',
				'font_size'                  => array( 'amount' => '13', 'unit' => 'px' ),
				'padding_top'                  => array( 'amount' => '15', 'unit' => 'px' ),
				'padding_right'                  => array( 'amount' => '50', 'unit' => 'px' ),
				'padding_bottom'                  => array( 'amount' => '15', 'unit' => 'px' ),
				'padding_left'                  => array( 'amount' => '25', 'unit' => 'px' ),
			),
    );
	
	
    $controls['pro_hover_sub_nav_font'] = array(
        'name'       => 'pro_hover_sub_nav_font',
		'type'        => 'font',
        'title'      =>  __('Sub-Navigation Hover', 'progression'),
        'tab'        => 'pro-nav-headings',
        'properties' => array( 'selector'   => '.sf-menu li.sfHover li a:hover, .sf-menu li.sfHover li.sfHover a, .sf-menu li.sfHover li li a:hover, .sf-menu li.sfHover li.sfHover li.sfHover a,  .sf-menu li.sfHover li li li a:hover, .sf-menu li.sfHover li.sfHover li.sfHover a:hover, .sf-menu li.sfHover li.sfHover li.sfHover li.sfHover a, .sf-menu li.sfHover li li li li a:hover, .sf-menu li.sfHover li.sfHover li.sfHover li.sfHover a:hover, .sf-menu li.sfHover li.sfHover li.sfHover li.sfHover li.sfHover a, .sf-menu li.sfHover li li li li li a:hover, .sf-menu li.sfHover li.sfHover li.sfHover li.sfHover li.sfHover a:hover, .sf-menu li.sfHover li.sfHover li.sfHover li.sfHover li.sfHover li.sfHover a, ul.mobile-menu-pro li ul a:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#ffffff',
				'background_color'			=> 'rgba(255,255,255,  0.02)',
			),
    );
	
	
    $controls['pro_mega_menu_font'] = array(
        'name'       => 'pro_mega_menu_font',
		'type'        => 'font',
        'title'      =>  __('Mega Menu Heading', 'progression'),
        'tab'        => 'pro-nav-headings',
        'properties' => array( 'selector'   => '.sf-mega h2.mega-menu-heading' ),
		'default' => array(
			'subset'                     => 'latin',
			'font_weight_style'                => '700',
			'font_color'                 => '#ffffff',
			'text_transform'             => 'uppercase',
			'font_size'                  => array( 'amount' => '15', 'unit' => 'px' ),
			'padding_top'                  => array( 'amount' => '20', 'unit' => 'px' ),
			'padding_right'                  => array( 'amount' => '25', 'unit' => 'px' ),
			'padding_bottom'                  => array( 'amount' => '10', 'unit' => 'px' ),
			'padding_left'                  => array( 'amount' => '25', 'unit' => 'px' ),
			),
    );
	
	
	
	
	
	
	/* Slider Revolution Headings */
    $controls['pro_slider_heading'] = array(
        'name'       => 'pro_slider_heading',
		'type'        => 'font',
        'title'      =>  __('Slider Heading', 'progression'),
        'tab'        => 'pro-slider-headings',
        'properties' => array( 'selector'   => '#pro-home-slider .pro-heading-light, #pro-home-slider .pro-heading-light a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '300',
				'font_color'                 => '#ffffff',
				'line_height'                => '1.2',
				'font_size'                  => array( 'amount' => '57', 'unit' => 'px' ),
			),
    );
	
	
    $controls['pro_slider_sub_heading'] = array(
        'name'       => 'pro_slider_sub_heading',
		'type'        => 'font',
        'title'      =>  __('Slider Subtitle', 'progression'),
        'tab'        => 'pro-slider-headings',
        'properties' => array( 'selector'   => '#pro-home-slider .pro-text-light, #pro-home-slider .pro-text-light a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
				'line_height'                => '1',
				'font_size'                  => array( 'amount' => '21', 'unit' => 'px' ),
			),
    );
	
	
    $controls['pro_slider_btn'] = array(
        'name'       => 'pro_slider_btn',
		'type'        => 'font',
        'title'      =>  __('Slider Button', 'progression'),
        'tab'        => 'pro-slider-headings',
        'properties' => array( 'selector'   => '#pro-home-slider .pro-button a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
				'background_color'           => '#4abd92',
				'line_height'                => '1',
				'text_transform'             => 'uppercase',
				'letter_spacing'             => array( 'amount' => '1.4', 'unit' => 'px' ),
				'font_size'                  => array( 'amount' => '14', 'unit' => 'px' ),				
				'padding_top'                  => array( 'amount' => '24', 'unit' => 'px' ),
				'padding_right'                  => array( 'amount' => '32', 'unit' => 'px' ),
				'padding_bottom'                  => array( 'amount' => '25', 'unit' => 'px' ),
				'padding_left'                  => array( 'amount' => '32', 'unit' => 'px' ),
				
			),
    );
	
	
    $controls['pro_slider_btn_hover'] = array(
        'name'       => 'pro_slider_btn_hover',
		'type'        => 'font',
        'title'      =>  __('Slider Button Hover', 'progression'),
        'tab'        => 'pro-slider-headings',
        'properties' => array( 'selector'   => '#pro-home-slider .pro-button a:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#ffffff',
				'background_color'           => '#343638',
				'line_height'                => '1',
				'font_size'                  => array( 'amount' => '14', 'unit' => 'px' ),
			),
    );
	
    $controls['pro_icon_btn_hover'] = array(
        'name'       => 'pro_icon_btn_hover',
		'type'        => 'font',
        'title'      =>  __('Slider Icon', 'progression'),
        'tab'        => 'pro-slider-headings',
        'properties' => array( 'selector'   => '#pro-home-slider a i' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#1b1b1b',
				'background_color'           => '#ffffff',
				'line_height'                => '1',
				'font_size'                  => array( 'amount' => '17', 'unit' => 'px' ),
			),
    );
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* Sidebar Fonts */
    $controls['pro_sidebar_title'] = array(
        'name'       => 'pro_sidebar_title',
		'type'        => 'font',
        'title'      =>  __('Sidebar Heading', 'progression'),
        'tab'        => 'pro-sidebar-fonts',
        'properties' => array( 'selector'   => '#sidebar h5.widget-title' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#505045',
				'line_height'                => '1',
				'text_transform'             => 'uppercase',
				'font_weight_style'                => '400',
				'font_size'                  => array( 'amount' => '20', 'unit' => 'px' ),
			),
    );
    $controls['pro_sidebar_link'] = array(
        'name'       => 'pro_sidebar_link',
		'type'        => 'font',
        'title'      =>  __('Sidebar Link', 'progression'),
        'tab'        => 'pro-sidebar-fonts',
        'properties' => array( 'selector'   => '#sidebar a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#585858',
			),
    );
    $controls['pro_sidebar_link_hover'] = array(
        'name'       => 'pro_sidebar_link_hover',
		'type'        => 'font',
        'title'      =>  __('Sidebar Link Hover', 'progression'),
        'tab'        => 'pro-sidebar-fonts',
        'properties' => array( 'selector'   => '#sidebar a:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#696969',
			),
    );
	
	
	
	
	/* FOOTER Fonts */
    $controls['pro_footer_default'] = array(
        'name'       => 'pro_footer_default',
		'type'        => 'font',
        'title'      =>  __('Footer Fonts', 'progression'),
        'tab'        => 'pro-footer-fonts',
        'properties' => array( 'selector'   => 'footer#site-footer' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',				
				'font_weight_style'                => '300',
				'font_color'                 => '#ffffff',
			),
    );
	
	
    $controls['pro_footer_heading'] = array(
        'name'       => 'pro_footer_heading',
		'type'        => 'font',
        'title'      =>  __('Footer Heading', 'progression'),
        'tab'        => 'pro-footer-fonts',
        'properties' => array( 'selector'   => 'h6.widget-title' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_weight_style'                => '600',
				'font_color'                 => '#ffffff',
				'text_transform'             => 'uppercase',
			),
    );
	
	
	
    $controls['pro_footer_link'] = array(
        'name'       => 'pro_footer_link',
		'type'        => 'font',
        'title'      =>  __('Footer Link', 'progression'),
        'tab'        => 'pro-footer-fonts',
        'properties' => array( 'selector'   => 'footer#site-footer a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
			),
    );
	
	
    $controls['pro_footer_link_hover'] = array(
        'name'       => 'pro_footer_link_hover',
		'type'        => 'font',
        'title'      =>  __('Footer Link Hover', 'progression'),
        'tab'        => 'pro-footer-fonts',
        'properties' => array( 'selector'   => 'footer#site-footer a:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#4abd92',
			),
    );
	
	
    $controls['pro_footer_copyright'] = array(
        'name'       => 'pro_footer_copyright',
		'type'        => 'font',
        'title'      =>  __('Copyright', 'progression'),
        'tab'        => 'pro-footer-fonts',
        'properties' => array( 'selector'   => '#copyright-text-pro' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '300',
				'font_color'                 => 'rgba(255,255,255, 0.2)',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '11', 'unit' => 'px' ),
				'padding_top'                => array( 'amount' => '32', 'unit' => 'px' ),
				'padding_bottom'             => array( 'amount' => '32', 'unit' => 'px' )
			),
    );
	
	
	
	
    $controls['pro_footer_nav'] = array(
        'name'       => 'pro_footer_nav',
		'type'        => 'font',
        'title'      =>  __('Footer Navigation', 'progression'),
        'tab'        => 'pro-footer-fonts',
        'properties' => array( 'selector'   => 'footer#site-footer #footer-right-pro ul li a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
			),
    );
	
	
    $controls['pro_footer_nav_hover'] = array(
        'name'       => 'pro_footer_nav_hover',
		'type'        => 'font',
        'title'      =>  __('Footer Navigation Hover', 'progression'),
        'tab'        => 'pro-footer-fonts',
        'properties' => array( 'selector'   => 'footer#site-footer #footer-right-pro ul li a:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#AEAEAE',
			),
    );
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* Button Font */
    $controls['pro_default_btn'] = array(
        'name'       => 'pro_default_btn',
		'type'        => 'font',
        'title'      =>  __('Default Button', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'a.pro-button-shortcode.default-style-pro' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
				'background_color'           => '#4abd92',
				'line_height'                => '1',
				'text_decoration'            => 'none',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '12', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '1.4', 'unit' => 'px' ),		
				'padding_top'          	     => array( 'amount' => '24', 'unit' => 'px' ),
				'padding_right'              => array( 'amount' => '32', 'unit' => 'px' ),
				'padding_bottom'             => array( 'amount' => '25', 'unit' => 'px' ),
				'padding_left'               => array( 'amount' => '32', 'unit' => 'px' )
			),
    );
    $controls['pro_default_btn_hover'] = array(
        'name'       => 'pro_default_btn_hover',
		'type'        => 'font',
        'title'      =>  __('Default Button Hover', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'a.pro-button-shortcode.default-style-pro:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#ffffff',
				'background_color'           => '#343638',
			),
    );
    $controls['pro_dark_btn'] = array(
        'name'       => 'pro_dark_btn',
		'type'        => 'font',
        'title'      =>  __('Dark Button', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'a.pro-button-shortcode.dark-style-pro' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
				'background_color'           => '#343638',
				'line_height'                => '1',
				'text_decoration'            => 'none',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '12', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '1.4', 'unit' => 'px' ),		
				'padding_top'          	     => array( 'amount' => '24', 'unit' => 'px' ),
				'padding_right'              => array( 'amount' => '32', 'unit' => 'px' ),
				'padding_bottom'             => array( 'amount' => '25', 'unit' => 'px' ),
				'padding_left'               => array( 'amount' => '32', 'unit' => 'px' )
			),
    );
    $controls['pro_dark_btn_hover'] = array(
        'name'       => 'pro_dark_btn_hover',
		'type'        => 'font',
        'title'      =>  __('Dark Button Hover', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'a.pro-button-shortcode.dark-style-pro:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#ffffff',
				'background_color'           => '#4abd92',
			),
    );
    $controls['pro_modern_btn'] = array(
        'name'       => 'pro_modern_btn',
		'type'        => 'font',
        'title'      =>  __('Modern Button', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'a.pro-button-shortcode.modern-style-pro' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
				'background_color'           => '#7e7e6a',
				'line_height'                => '1',
				'text_decoration'            => 'none',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '12', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '1.4', 'unit' => 'px' ),		
				'padding_top'          	     => array( 'amount' => '24', 'unit' => 'px' ),
				'padding_right'              => array( 'amount' => '32', 'unit' => 'px' ),
				'padding_bottom'             => array( 'amount' => '25', 'unit' => 'px' ),
				'padding_left'               => array( 'amount' => '32', 'unit' => 'px' )
			),
    );
    $controls['pro_modern_btn_hover'] = array(
        'name'       => 'pro_modern_btn_hover',
		'type'        => 'font',
        'title'      =>  __('Modern Button Hover', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'a.pro-button-shortcode.modern-style-pro:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#ffffff',
				'background_color'           => '#343638',
			),
    );
	
	
    $controls['pro_light_btn'] = array(
        'name'       => 'pro_light_btn',
		'type'        => 'font',
        'title'      =>  __('Modern Button', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'a.pro-button-shortcode.light-style-pro' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#1b1b1b',
				'background_color'           => '#ffffff',
				'line_height'                => '1',
				'text_decoration'            => 'none',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '12', 'unit' => 'px' ),
				'letter_spacing'             => array( 'amount' => '1.4', 'unit' => 'px' ),		
				'padding_top'          	     => array( 'amount' => '24', 'unit' => 'px' ),
				'padding_right'              => array( 'amount' => '32', 'unit' => 'px' ),
				'padding_bottom'             => array( 'amount' => '25', 'unit' => 'px' ),
				'padding_left'               => array( 'amount' => '32', 'unit' => 'px' )
			),
    );
    $controls['pro_light_btn_hover'] = array(
        'name'       => 'pro_light_btn_hover',
		'type'        => 'font',
        'title'      =>  __('Modern Button Hover', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'a.pro-button-shortcode.light-style-pro:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#ffffff',
				'background_color'           => '#343638',
			),
    );
	
	
	
    $controls['pro_shop_btn'] = array(
        'name'       => 'pro_shop_btn',
		'type'        => 'font',
        'title'      =>  __('Shop Button', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'body  .woocommerce .form-row input.button, body .woocommerce .return-to-shop a.button, .woocommerce .cart_totals .wc-proceed-to-checkout a.button, .woocommerce .woocommerce-checkout input.button, #reviews #respond p.form-submit input.submit,
#content-pro ul li.product .button, body #content-pro #content .button, .woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#ffffff',
				'background_color'           => '#7e7e6a',
			),
    );
    $controls['pro_shop_btn_hover'] = array(
        'name'       => 'pro_shop_btn_hover',
		'type'        => 'font',
        'title'      =>  __('Shop Button Hover', 'progression'),
        'tab'        => 'pro-button-fonts',
        'properties' => array( 'selector'   => 'body  .woocommerce .form-row input.button:hover, body .woocommerce .return-to-shop a.button:hover, .woocommerce .cart_totals .wc-proceed-to-checkout a.button:hover, .woocommerce .woocommerce-checkout input.button:hover,
#content-pro ul li.product .button:hover, body #content-pro #content .button:hover, #reviews #respond p.form-submit input.submit:hover' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_color'                 => '#ffffff',
				'background_color'           => '#343638',
			),
    );
	
	
	
	
	
	
	/* Wine Styles */
    $controls['pro_wine_heading'] = array(
        'name'       => 'pro_wine_heading',
		'type'        => 'font',
        'title'      =>  __('Wine Archive Headings', 'progression'),
        'tab'        => 'pro-wine-fonts',
        'properties' => array( 'selector'   => 'body h6.wine-title-pro, body h6.wine-title-pro a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#505045',
				'line_height'                => '1.4',
				'text_transform'             => 'uppercase',
				'letter_spacing'             => array( 'amount' => '1.4', 'unit' => 'px' ),
				'font_size'                  => array( 'amount' => '17', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '45', 'unit' => 'px' ),
				'margin_top'                 => array( 'amount' => '18', 'unit' => 'px' )
			),
    );
    $controls['pro_wine_header'] = array(
        'name'       => 'pro_wine_header',
		'type'        => 'font',
        'title'      =>  __('Wine Item Description', 'progression'),
        'tab'        => 'pro-wine-fonts',
        'properties' => array( 'selector'   => '.pro-sub-header' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => '700',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '14', 'unit' => 'px' ),
				'margin_top'                 => array( 'amount' => '15', 'unit' => 'px' ),				
				'margin_bottom'              => array( 'amount' => '20', 'unit' => 'px' )
			),
    );



	/* Event Styles */
    $controls['Heading_pro_event_heading'] = array(
        'name'       => 'Heading_pro_event_heading',
		'type'        => 'font',
        'title'      =>  __('Event Archive Headings', 'progression'),
        'tab'        => 'pro-event-fonts',
        'properties' => array( 'selector'   => 'h5.event-title-pro a' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'lato',
				'font_name'                  => 'Lato',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#7e7e6a',
				'line_height'                => '1.4',
				'text_transform'             => 'uppercase',
				'font_size'                  => array( 'amount' => '25', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '8', 'unit' => 'px' )
			),
    );
	
    $controls['pro_event_date'] = array(
        'name'       => 'pro_event_date',
		'type'        => 'font',
        'title'      =>  __('Single Event Date', 'progression'),
        'tab'        => 'pro-event-fonts',
        'properties' => array( 'selector'   => 'body.single-event .entry-meta .entry-date' ),
		'default' => array(
				'subset'                     => 'latin',
				'font_id'                    => 'crimson_text',
				'font_name'                  => 'Crimson Text',
				'font_style'                 => 'italic',
				'font_weight_style'                => 'normal',
				'font_color'                 => '#4abd92',
				'background_color'           => '#f5f5f5',
				'font_size'                  => array( 'amount' => '20', 'unit' => 'px' ),
				'margin_bottom'              => array( 'amount' => '15', 'unit' => 'px' ),
				'padding_top'          	     => array( 'amount' => '12', 'unit' => 'px' ),
				'padding_right'              => array( 'amount' => '12', 'unit' => 'px' ),
				'padding_bottom'             => array( 'amount' => '10', 'unit' => 'px' ),
				'padding_left'               => array( 'amount' => '15', 'unit' => 'px' )
			),
    );
	
	
	
	
	
	
    
	// Return the controls.
    return $controls;
}
add_filter( 'tt_font_get_option_parameters', 'pro_add_control_to_tab' );