<?php
/**
 * WooCommerce Functions
 *
 */

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	
	function loop_columns() {
		$col_count_progression = esc_attr(get_theme_mod('woocommerce_columns_pro', '3'));
		return $col_count_progression; // 3 products per row
	}
}


// Display 24 products per page. Goes in functions.php
$post_count_pro = esc_attr(get_theme_mod('woocommerce_post_count_pro', '12'));
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return ' . $post_count_pro . ';' ), 20 );



// Disable small screen css
add_filter( 'woocommerce_enqueue_styles', 'pro_woo_dequeue_styles' );
function pro_woo_dequeue_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}



/**
 * Manage WooCommerce styles and scripts.
 */
function grd_woocommerce_script_cleaner() {
	wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
	wp_dequeue_script( 'prettyPhoto' );
	wp_dequeue_script( 'prettyPhoto-init' );
}
add_action( 'wp_enqueue_scripts', 'grd_woocommerce_script_cleaner', 99 );



add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
	  $col_count_progression = esc_attr(get_theme_mod('woocommerce_columns_pro', '3'));
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 2 columns
	return $args;
}
