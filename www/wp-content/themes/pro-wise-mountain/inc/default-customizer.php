<?php
/**
 * progression Theme Customizer
 *
 * @package progression
 */
require get_template_directory() . '/inc/customizer/new-controls.php';
require get_template_directory() . '/inc/customizer/typography-controls.php';


/* Remove Default Theme Customizer Panels that aren't useful */
function pro_change_default_customizer_panels ( $wp_customize ) {
	$wp_customize->remove_section("themes"); //Remove Active Theme + Theme Changer
	
	$wp_customize->remove_section("title_tagline"); // Remove Title & Tagline Section
    $wp_customize->remove_section("static_front_page"); // Remove Front Page Section	
	$wp_customize->remove_section("nav"); // Remove Navigation Section
	
	// $wp_customize->get_section('themes')->priority = 500;
	// $wp_customize->remove_panel("widgets");  //Widgets Panel
}
add_action( "customize_register", "pro_change_default_customizer_panels" );


/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function progression_customizer( $wp_customize ) {
	
	
	/* Panel - General */
	$wp_customize->add_panel( 'general_panel_pro', array(
		'priority'    => 10,
        'title'       => __( 'General', 'progression' ),
    ) );
	
	/* Section - General - Site Logo */
	$wp_customize->add_section( 'section_logo_pro', array(
		'title'          => __( 'Site Logo', 'progression' ),
		'panel'          => 'general_panel_pro', // Not typically needed.
		'priority'       => 10,
	) );
	
	/* Setting - General - Site Logo */
	$wp_customize->add_setting( 'pro_theme_logo' ,array(
		'default' => get_template_directory_uri().'/images/logo.png',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'pro_theme_logo', array(
		'section' => 'section_logo_pro',
		'priority'   => 10,
		))
	);
	
	/* Setting - General - Site Logo */
	$wp_customize->add_setting('pro_theme_logo_width',array(
		'default' => '250',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new Progression_Controls_Slider_Control($wp_customize, 'pro_theme_logo_width', array(
		'label'    => __( 'Logo Width (px)', 'progression' ),
		'section'  => 'section_logo_pro',
		'priority'   => 15,
		'choices'     => array(
			'min'  => 0,
			'max'  => 1200,
			'step' => 1
		),
	) ) );
	
	/* Setting - General - Site Logo */
	$wp_customize->add_setting('theme_logo_margin_top',array(
		'default' => '26',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new Progression_Controls_Slider_Control($wp_customize, 'theme_logo_margin_top', array(
		'label'    => __( 'Logo Margin Top (px)', 'progression' ),
		'section'  => 'section_logo_pro',
		'priority'   => 20,
		'choices'     => array(
			'min'  => 0,
			'max'  => 150,
			'step' => 1
		),
	) ) );
	
	/* Setting - General - Site Logo */
	$wp_customize->add_setting('theme_logo_margin_bottom',array(
		'default' => '30',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new Progression_Controls_Slider_Control($wp_customize, 'theme_logo_margin_bottom', array(
		'label'    => __( 'Logo Margin Bottom (px)', 'progression' ),
		'section'  => 'section_logo_pro',
		'priority'   => 25,
		'choices'     => array(
			'min'  => 0,
			'max'  => 150,
			'step' => 1
		),
	) ) );
	
	/* Setting - General - Site Logo */
	$wp_customize->add_setting( 'pro_theme_fav_icon' ,array(
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'pro_theme_fav_icon', array(
		'label'    => __( 'Favicon', 'progression' ),
		'description'    => __( 'File must be .png or .ico format. Recommended Dimensions: 32px by 32px', 'progression' ),
		'section' => 'section_logo_pro',
		'priority'   => 40,
		))
	);
	
	
	/* Section - General - Footer */
	$wp_customize->add_section( 'section_footer_general_pro', array(
		'title'          => __( 'Footer', 'progression' ),
		'panel'          => 'general_panel_pro', // Not typically needed.
		'priority'       => 30,
	) );
	
	/* Setting - General - Footer */
	$wp_customize->add_setting( 'footer_copyright_pro' ,array(
		'default' => '2015 All Rights Reserved. Developed by ProgressionStudios',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( 'footer_copyright_pro', array(
		'label'          => __( 'Copyright Text', 'progression' ),
		'section' => 'section_footer_general_pro',
		'type' => 'text',
		'priority'   => 10,
		)
	);
	
	/* Setting - General - Footer */
	$wp_customize->add_setting( 'pro_footer_align', array(
		'default' => 'footer-center-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control('pro_footer_align', array(
		'label'    => __( 'Copyright Text Alignment', 'progression' ),
		'section'  => 'section_footer_general_pro',
		'type' => 'select',
		'priority'   => 20,
		'choices'     => array(
			'footer-left-pro' => __( 'Left', 'progression' ),
			'footer-center-pro' => __( 'Center', 'progression' ),
			'footer-right-pro' => __( 'Right', 'progression' ),
		),
	)  );

	/* Setting - General - Footer */
	$wp_customize->add_setting( 'pro_footer_widget_align', array(
		'default' => 'footer-center-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control('pro_footer_widget_align', array(
		'label'    => __( 'Footer Widgets Text Alignment', 'progression' ),
		'section'  => 'section_footer_general_pro',
		'type' => 'select',
		'priority'   => 20,
		'choices'     => array(
			'footer-left-pro' => __( 'Left', 'progression' ),
			'footer-center-pro' => __( 'Center', 'progression' ),
			'footer-right-pro' => __( 'Right', 'progression' ),
		),
	)  );	
	
	/* Setting - General - Footer */
	$wp_customize->add_setting( 'pro_widget_count', array(
		'default' => 'footer-1-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'pro_widget_count', array(
		'label'    => __( 'Footer Widget Count', 'progression' ),
		'section'  => 'section_footer_general_pro',
		'priority'   => 30,
		'choices'     => array(
			'footer-1-pro' => __( '1', 'progression' ),
			'footer-2-pro' => __( '2', 'progression' ),
			'footer-3-pro' => __( '3', 'progression' ),
			'footer-4-pro' => __( '4', 'progression' ),
		),
	) ) );
	

	
	
	/* Section - General - Scroll To Top */
	$wp_customize->add_section( 'section_scroll_pro', array(
		'title'          => __( 'Scroll To Top Button', 'progression' ),
		'panel'          => 'general_panel_pro', // Not typically needed.
		'priority'       => 70,
	) );
	
	/* Setting - General - Scroll To Top */
	$wp_customize->add_setting( 'pro_scroll_top', array(
		'default' => 'scroll-on-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'pro_scroll_top', array(
		'label'    => __( 'Scroll To Top Button', 'progression' ),
		'section'  => 'section_scroll_pro',
		'priority'   => 10,
		'choices'     => array(
			'scroll-on-pro' => __( 'On', 'progression' ),
			'scroll-off-pro' => __( 'Off', 'progression' ),
		),
	) ) );
	
	/* Setting - General - Scroll To Top */
	$wp_customize->add_setting( 'pro_scroll_color', array(
		'default' => '#ffffff',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pro_scroll_color', array(
		'label'    => __( 'Color', 'progression' ),
		'section'  => 'section_scroll_pro',
		'priority'   => 15,
	) ) );
	

	/* Setting - General - Scroll To Top */
	$wp_customize->add_setting( 'pro_scroll_bg_color', array(
		'default' => '#4abd92',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'pro_scroll_bg_color', array(
		'label'    => __( 'Background', 'progression' ),
		'default' => '#4abd92',
		'section'  => 'section_scroll_pro',
		'priority'   => 20,
	) ) );
	
	/* Setting - General - Scroll To Top */
	$wp_customize->add_setting( 'pro_scroll_border_color', array(
		'default' => 'rgba(255,255,255,  0)',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'pro_scroll_border_color', array(
		'label'    => __( 'Border', 'progression' ),
		'default' => 'rgba(255,255,255,  0))',
		'section'  => 'section_scroll_pro',
		'priority'   => 25,
	) ) );
	
	/* Setting - General - Scroll To Top */
	$wp_customize->add_setting( 'pro_scroll_hvr_color', array(
		'default' => '#fffffff',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'pro_scroll_hvr_color', array(
		'label'    => __( 'Hover Color', 'progression' ),
		'section'  => 'section_scroll_pro',
		'priority'   => 30,
	) ) );
	
	
	/* Setting - General - Scroll To Top */
	$wp_customize->add_setting( 'pro_scroll_hvr_bg_color', array(
		'default' => '#4abd92',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'pro_scroll_hvr_bg_color', array(
		'label'    => __( 'Hover Background', 'progression' ),
		'default' => '#4abd92',
		'section'  => 'section_scroll_pro',
		'priority'   => 40,
	) ) );
	
	/* Setting - General - Scroll To Top */
	$wp_customize->add_setting( 'pro_scroll_hvr_border_color', array(
		'default' => 'rgba(255,255,255,  0)',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'pro_scroll_hvr_border_color', array(
		'label'    => __( 'Border', 'progression' ),
		'default' => 'rgba(255,255,255,  0))',
		'section'  => 'section_scroll_pro',
		'priority'   => 50,
	) ) );
	
	
	
	
	
	
	
	
	
	
	
	
	/* Section - General - Site Title & Tagline */
	$wp_customize->add_section( 'site_title_pro', array(
		'title'          => __( 'Site Title & Tagline', 'progression' ),
		'panel'          => 'general_panel_pro', // Not typically needed.
		'priority'       => 85,
	) );
	
	/* Setting - General - Site Title & Tagline */
	$wp_customize->add_setting(  'blogname' , array(
	        'default'    => get_option( 'blogname' ),
			'sanitize_callback' => 'progression_sanitize_text',
			'type' => 'option',
			'capability' => 'manage_options',
	    )
	);
	$wp_customize->add_control( 'blogname',	array(
	   	 	'label' => __('Site Title', 'progression'), 
			'section' => 'site_title_pro',
			'priority' => 10,
	    )
	);
	
	/* Setting - General - Site Title & Tagline */
	$wp_customize->add_setting(  'blogdescription' , array(
	        'default'    => get_option( 'blogdescription' ),
			'sanitize_callback' => 'progression_sanitize_text',
			'capability' => 'manage_options',
			'type' => 'option',
	    )
	);
	$wp_customize->add_control( 'blogdescription',	array(
	   	 	'label' => __('Tagline', 'progression'), 
			'section' => 'site_title_pro',
			'priority' => 15,
	    )
	);
	
	/* Section - General - Navigation */
	/* Nav Menus */
			$locations      = get_registered_nav_menus();
			$menus          = wp_get_nav_menus();
			$num_locations  = count( array_keys( $locations ) );
			if ( 1 == $num_locations ) {
				$description = __( 'Your theme supports one menu. Select which menu you would like to use.', 'progression' );
			} else {
				$description = sprintf( _n( 'Your theme supports %s menu. Select which menu appears in each location.', 'Your theme supports %s menus. Select which menu appears in each location.', 'progression', $num_locations ), number_format_i18n( $num_locations ) );
			}
			$wp_customize->add_section( 'nav', array(
				'title'          => __( 'Navigation', 'progression' ),
				'theme_supports' => 'menus',
				'panel'     => 'general_panel_pro',
				'priority'       => 90,
				'description'    => $description . "\n\n" . __( 'You can edit your menu content on the Menus screen in the Appearance section.', 'progression' ),
			) );
			if ( $menus ) {
				$choices = array( '' => __( '&mdash; Select &mdash;', 'progression' ) );
				foreach ( $menus as $menu ) {
					$choices[ $menu->term_id ] = wp_html_excerpt( $menu->name, 40, '&hellip;', 'progression' );
				}
				foreach ( $locations as $location => $description ) {
					$menu_setting_id = "nav_menu_locations[{$location}]";
					$wp_customize->add_setting( $menu_setting_id, array(
						'sanitize_callback' => 'absint',
						'theme_supports'    => 'menus',
					) );
					$wp_customize->add_control( $menu_setting_id, array(
						'label'   => $description,
						'section' => 'nav',
						'type'    => 'select',
						'choices' => $choices,
					) );
				}
			}
	
	
	/* Section - General - Static Front Page */
	if ( get_pages() ) {
	//SITE TITLE & TAGLINE OPTION
	$wp_customize->add_section( 'static_front_page_progression',
	   	array(
	       	'title' => __('Static Front Page', 'progression'),
			'description'    => __( 'Your theme supports a static front page.', 'progression' ),
	       	'priority' => 100,
			'panel'     => 'general_panel_pro',
	    	)
	);
	
		$wp_customize->add_setting( 'show_on_front', array(
			'sanitize_callback' => 'progression_sanitize_text',
					'default'        => get_option( 'show_on_front' ),
					'capability'     => 'manage_options',
					'type'           => 'option',
		) );
		$wp_customize->add_control( 'show_on_front', array(
			'label'   => __( 'Front page displays', 'progression' ),
			'section' => 'static_front_page_progression',
			'type'    => 'radio',
			'choices' => array(
				'posts' => __( 'Your latest posts', 'progression' ),
				'page'  => __( 'A static page',  'progression' ),
			),
		) );
		$wp_customize->add_setting( 'page_on_front', array(
			'type'       => 'option',
			'capability' => 'manage_options',
			'sanitize_callback' => 'progression_sanitize_text',
		) );
		$wp_customize->add_control( 'page_on_front', array(
			'label'      => __( 'Front page', 'progression' ),
			'section'    => 'static_front_page_progression',
			'type'       => 'dropdown-pages',
		) );
		$wp_customize->add_setting( 'page_for_posts', array(
			'type'           => 'option',
			'capability'     => 'manage_options',
			'sanitize_callback' => 'progression_sanitize_text',
		) );
		$wp_customize->add_control( 'page_for_posts', array(
			'label'      => __( 'Posts page', 'progression' ),
			'section'    => 'static_front_page_progression',
			'type'       => 'dropdown-pages',
		) );
	}
	
	
	
	
	
	
	
	/* Panel - Backgrounds */
	$wp_customize->add_panel( 'typography_panel_pro', array(
		'priority'    => 15,
        'title'       => __( 'Typography', 'progression' ),
    ) );

	
	
	/* Section - Backgrounds - Navigation Background */
	$wp_customize->add_section( 'section_highlight_pro', array(
		'title'          => __( 'Highlight Color', 'progression' ),
		'panel'          => 'backgrounds_panel_pro', // Not typically needed.
		'priority'       => 5,
	) );
	
	/* Setting - Backgrounds - Navigation Background */
	$wp_customize->add_setting( 'highlight_color_pro', array(
		'default'	=> '#4abd92',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'highlight_color_pro', array(
		'label'    => __( 'Theme Highlight Color', 'progression' ),
		'default'	=> '#4abd92',
		'section'  => 'section_highlight_pro',
		'priority'   => 10,
		)
	) );
	/* Setting - Backgrounds - Navigation Background */
	$wp_customize->add_setting( 'sec_highlight_color_pro', array(
		'default'	=> '#253034',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'sec_highlight_color_pro', array(
		'label'    => __( 'Theme Secondary Color', 'progression' ),
		'default'	=> '#253034',
		'section'  => 'section_highlight_pro',
		'priority'   => 10,
		)
	) );	
	

	
	/* Panel - Backgrounds */
	$wp_customize->add_panel( 'backgrounds_panel_pro', array(
		'priority'    => 20,
        'title'       => __( 'Backgrounds', 'progression' ),
    ) );
	
	
	/* Section - Backgrounds - Navigation Background */
	$wp_customize->add_section( 'section_header_pro', array(
		'title'          => __( 'Header Background', 'progression' ),
		'panel'          => 'backgrounds_panel_pro', // Not typically needed.
		'priority'       => 11,
	) );
	
	/* Setting - Backgrounds - Navigation Background */
	$wp_customize->add_setting( 'header_bg_color_pro', array(
		'default'	=> '',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'header_bg_color_pro', array(
		'label'    => __( 'Header Background Color', 'progression' ),
		'default'	=> '',
		'section'  => 'section_header_pro',
		'priority'   => 10,
		)
	) );
		
		$wp_customize->add_setting( 'sticky_header_bg_color_pro', array(
			'default'	=> '#253034',
			'sanitize_callback' => 'progression_sanitize_text',
		) );
		$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'sticky_header_bg_color_pro', array(
			'label'    => __( 'Sticky Header Background Color', 'progression' ),
			'default'	=> '#253034',
			'section'  => 'section_header_pro',
			'priority'   => 15,
			)
		) );
	
	
	/* Section - Backgrounds - Navigation Background */
	$wp_customize->add_section( 'section_navigation_pro', array(
		'title'          => __( 'Navigation Background', 'progression' ),
		'panel'          => 'backgrounds_panel_pro', // Not typically needed.
		'priority'       => 10,
	) );
	
	/* Setting - Backgrounds - Navigation Background */
	$wp_customize->add_setting( 'nav_bg_color_pro', array(
		'default'	=> 'rgba(24,26,30,0)',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'nav_bg_color_pro', array(
		'label'    => __( 'Navigation Background Color', 'progression' ),
		'default'	=> 'rgba(24,26,30,0)',
		'section'  => 'section_navigation_pro',
		'priority'   => 10,
		)
	) );
	/* Setting - Backgrounds - Navigation Background */
	$wp_customize->add_setting( 'mobi_nav_bg_color_pro', array(
		'default'	=> 'rgba(24,26,30,0.99)',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'mobi_nav_bg_color_pro', array(
		'label'    => __( 'Mobile Navigation Background Color', 'progression' ),
		'default'	=> 'rgba(24,26,30,0.99)',
		'section'  => 'section_navigation_pro',
		'priority'   => 10,
		)
	) );	
	/* Setting - Backgrounds - Navigation Background */
	$wp_customize->add_setting( 'nav_bg_image_pro' ,array(
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'nav_bg_image_pro', array(
		'label'    => __( 'Navigation Background Image', 'progression' ),
		'section' => 'section_navigation_pro',
		'priority'   => 20,
		)
	) );
	/* Setting - Backgrounds - Navigation Background */
	$wp_customize->add_setting( 'nav_bg_cover_pro', array(
		'default' => 'cover-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'nav_bg_cover_pro', array(
		'label'    => __( 'Navigation Image Cover or Pattern', 'progression' ),
		'section'  => 'section_navigation_pro',
		'priority'   => 30,
		'choices'     => array(
			'cover-pro' => __( 'Cover', 'progression' ),
			'pattern-pro' => __( 'Pattern', 'progression' ),
		),)
	) );
	$wp_customize->add_setting( 'sub_navborder_pro', array(
		'default'	=> '#4abd92',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'sub_navborder_pro', array(
		'label'    => __( 'Sub-Navigation Border Top', 'progression' ),
		'default'	=> '#4abd92',
		'section'  => 'section_navigation_pro',
		'priority'   => 40,
		)
	) );
	$wp_customize->add_setting( 'sub_nav_bg_color_pro', array(
		'default'	=> 'rgba(38,42,48,  0.99)',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'sub_nav_bg_color_pro', array(
		'label'    => __( 'Sub-Navigation Background', 'progression' ),
		'default'	=> 'rgba(38,42,48,  0.99)',
		'section'  => 'section_navigation_pro',
		'priority'   => 40,
		)
	) );
	
	
	
	
	
	
	
	

	/* Section - Backgrounds - Page Title Background */
	$wp_customize->add_section( 'section_title_pro', array(
		'title'          => __( 'Page Title Background', 'progression' ),
		'panel'          => 'backgrounds_panel_pro', // Not typically needed.
		'priority'       => 20,
	) );
	
	/* Setting - Backgrounds - Page Title Background */
	$wp_customize->add_setting( 'title_bg_color_pro', array(
		'default'	=> '#181a1e',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'title_bg_color_pro', array(
		'default'	=> '#181a1e',
		'label'    => __( 'Background Color', 'progression' ),
		'section'  => 'section_title_pro',
		'priority'   => 10,
		)
	) );
	/* Setting - Backgrounds - Page Title Background */
	$wp_customize->add_setting( 'title_bg_image_pro' ,array(
		'default' => get_template_directory_uri().'/images/page-title.jpg',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'title_bg_image_pro', array(
		'label'    => __( 'Background Image', 'progression' ),
		'section' => 'section_title_pro',
		'priority'   => 20,
		)
	) );
	/* Setting - Backgrounds - Page Title Background */
	$wp_customize->add_setting( 'title_bg_cover_pro', array(
		'default' => 'cover-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'title_bg_cover_pro', array(
		'label'    => __( 'Background Cover or Pattern', 'progression' ),
		'section'  => 'section_title_pro',
		'priority'   => 30,
		'choices'     => array(
			'cover-pro' => __( 'Cover', 'progression' ),
			'pattern-pro' => __( 'Pattern', 'progression' ),
		),)
	)  );
		
	/* Section - Backgrounds - Body Background */
	$wp_customize->add_section( 'section_body_pro', array(
		'title'          => __( 'Body Background', 'progression' ),
		'panel'          => 'backgrounds_panel_pro', // Not typically needed.
		'priority'       => 30,
	) );
	
	/* Setting - Backgrounds - Body Background */
	$wp_customize->add_setting( 'body_bg_color_pro', array(
		'default'	=> '#ffffff',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'body_bg_color_pro', array(
		'default'	=> '#ffffff',
		'label'    => __( 'Background Color', 'progression' ),
		'section'  => 'section_body_pro',
		'priority'   => 10,
		)
	) );
	/* Setting - Backgrounds - Body Background */
	$wp_customize->add_setting( 'body_bg_image_pro' ,array(
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'body_bg_image_pro', array(
		'label'    => __( 'Background Image', 'progression' ),
		'section' => 'section_body_pro',
		'priority'   => 20,
		)
	) );
	/* Setting - Backgrounds - Body Background */
	$wp_customize->add_setting( 'body_bg_cover_pro', array(
		'default' => 'cover-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'body_bg_cover_pro', array(
		'label'    => __( 'Background Cover or Pattern', 'progression' ),
		'section'  => 'section_body_pro',
		'priority'   => 30,
		'choices'     => array(
			'cover-pro' => __( 'Cover', 'progression' ),
			'pattern-pro' => __( 'Pattern', 'progression' ),
		),)
	) );
	
	/* Section - Backgrounds - Boxed Background */
	$wp_customize->add_section( 'section_boxed_pro', array(
		'title'          => __( 'Boxed Layout Background', 'progression' ),
		'panel'          => 'backgrounds_panel_pro', // Not typically needed.
		'priority'       => 40,
	) );
	
	/* Setting - Backgrounds - Boxed Background */
	$wp_customize->add_setting( 'boxed_bg_color_pro', array(
		'default'	=> '#ffffff',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'boxed_bg_color_pro', array(
		'default'	=> '#ffffff',
		'label'    => __( 'Background Color', 'progression' ),
		'section'  => 'section_boxed_pro',
		'priority'   => 10,
		)
	) );
	/* Setting - Backgrounds - Boxed Background */
	$wp_customize->add_setting( 'boxed_bg_image_pro' ,array(
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'boxed_bg_image_pro', array(
		'label'    => __( 'Background Image', 'progression' ),
		'section' => 'section_boxed_pro',
		'priority'   => 20,
		)
	) );
	/* Setting - Backgrounds - Boxed Background */
	$wp_customize->add_setting( 'boxed_bg_cover_pro', array(
		'default' => 'cover-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'boxed_bg_cover_pro', array(
		'label'    => __( 'Background Cover or Pattern', 'progression' ),
		'section'  => 'section_boxed_pro',
		'priority'   => 30,
		'choices'     => array(
			'cover-pro' => __( 'Cover', 'progression' ),
			'pattern-pro' => __( 'Pattern', 'progression' ),
		),)
	) );	
		
	/* Section - Backgrounds - Footer Background */
	$wp_customize->add_section( 'section_footer_pro', array(
		'title'          => __( 'Footer Background', 'progression' ),
		'panel'          => 'backgrounds_panel_pro', // Not typically needed.
		'priority'       => 60,
	) );
	/* Setting - Backgrounds - Footer Background */
	$wp_customize->add_setting( 'footer_bg_color_pro', array(
		'default'	=> '#253034',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'footer_bg_color_pro', array(
		'default'	=> '#253034',
		'label'    => __( 'Background Color', 'progression' ),
		'section'  => 'section_footer_pro',
		'priority'   => 10,
		)
	) );
	/* Setting - Backgrounds - Footer Background */
	$wp_customize->add_setting( 'footer_bg_image_pro' ,array(
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'footer_bg_image_pro', array(
		'label'    => __( 'Background Image', 'progression' ),
		'section' => 'section_footer_pro',
		'priority'   => 20,
		)
	) );
	/* Setting - Backgrounds - Footer Background */
	$wp_customize->add_setting( 'footer_bg_cover_pro', array(
		'default' => 'cover-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'footer_bg_cover_pro', array(
		'label'    => __( 'Background Cover or Pattern', 'progression' ),
		'section'  => 'section_footer_pro',
		'priority'   => 30,
		'choices'     => array(
			'cover-pro' => __( 'Cover', 'progression' ),
			'pattern-pro' => __( 'Pattern', 'progression' ),
		),)
	) );
		
		

	/* Section - Backgrounds - Container Backgrounds */
	$wp_customize->add_section( 'section_container_pro', array(
		'title'          => __( 'Container Backgrounds', 'progression' ),
		'panel'          => 'backgrounds_panel_pro', // Not typically needed.
		'priority'       => 50,
	) );
	/* Setting - Backgrounds - Container Backgrounds */
	$wp_customize->add_setting( 'sidebar_bg_pro', array(
		'default'	=> '#deded4',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'sidebar_bg_pro', array(
		'default'	=> '#deded4',
		'label'    => __( 'Sidebar Background', 'progression' ),
		'section'  => 'section_container_pro',
		'priority'   => 10,
		)
	) );

	/* Setting - Backgrounds - Container Backgrounds */
	$wp_customize->add_setting( 'shop_container_bg_pro', array(
		'default'	=> '#deded4',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Customize_Alpha_Color_Control($wp_customize, 'shop_container_bg_pro', array(
		'default'	=> '#deded4',
		'label'    => __( 'Shop Background', 'progression' ),
		'section'  => 'section_container_pro',
		'priority'   => 40,
		)
	) );
	
	
	
	
	
	
	
	
	
	
	/* Panel - Layout */
	$wp_customize->add_panel( 'layout_panel_pro', array(
		'priority'    => 40,
		'title'       => __( 'Layout', 'progression' ),
	) );
	
	/* Section - Layout - Global */
	$wp_customize->add_section( 'section_global_pro', array(
		'title'          => __( 'Global', 'progression' ),
		'panel'          => 'layout_panel_pro', // Not typically needed.
		'priority'       => 15,
	) );
	
	/* Setting - Layout - Global */
	$wp_customize->add_setting( 'pro_site_layout_wide', array(
		'default' => 'full-width-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'pro_site_layout_wide', array(
		'label'    => __( 'Site Layout', 'progression' ),
		'section'  => 'section_global_pro',
		'priority'   => 10,
		'choices'     => array(
			'full-width-pro' => __( 'Full Width', 'progression' ),
			'boxed-pro' => __( 'Boxed', 'progression' ),
		),
	) ) );

	/* Setting - Layout - Global */
	$wp_customize->add_setting( 'pro_global_comments', array(
		'default' => 'comment-on-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'pro_global_comments', array(
		'label'    => __( 'Page Comments', 'progression' ),
		'section'  => 'section_global_pro',
		'priority'   => 20,
		'choices'     => array(
			'comment-on-pro' => __( 'On', 'progression' ),
			'comment-off-pro' => __( 'Off', 'progression' ),
		),
	) ) );
	
	/* Section - Layout - Custom CSS */
	$wp_customize->add_section( 'section_css_pro', array(
		'title'          => __( 'Custom CSS', 'progression' ),
		'panel'          => 'layout_panel_pro', // Not typically needed.
		'priority'       => 150,
	) );
	
	/* Setting - Layout - Custom CSS */
	$wp_customize->add_setting( 'custom_css_pro' ,array(
		'default' => '',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( 'custom_css_pro', array(
		'description'          => __( 'Add-in any custom styles here', 'progression' ),
		'section' => 'section_css_pro',
		'type' => 'textarea',
		'priority'   => 10,
		)
	);
	
	
	
	/* Section - Layout - Header */
	$wp_customize->add_section( 'section_header_layout_pro', array(
		'title'          => __( 'Header', 'progression' ),
		'panel'          => 'layout_panel_pro', // Not typically needed.
		'priority'       => 40,
	) );
	
	
	/* Setting - Layout - Header */
	$wp_customize->add_setting( 'fixed_menu_pro', array(
		'default' => 'not-fixed-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'fixed_menu_pro', array(
		'label'    => __( 'Navigation Position', 'progression' ),
		'section'  => 'section_header_layout_pro',
		'priority'   => 10,
		'choices'     => array(
			'not-fixed-pro' => __( 'Normal Menu', 'progression' ),
			'fixed-pro' => __( 'Sticky Menu', 'progression' ),
		),
	) ) );

	/* Setting - Layout - Header */
	$wp_customize->add_setting( 'nav_alignment_pro', array(
		'default' => 'nav-right-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control('nav_alignment_pro', array(
		'label'    => __( 'Navigation Alignment', 'progression' ),
		'section'  => 'section_header_layout_pro',
		'type' => 'select',
		'priority'   => 20,
		'choices'     => array(
			'nav-left-pro' => __( 'Left', 'progression' ),
			'nav-right-pro' => __( 'Right', 'progression' ),
		),
	)  );

	/* Setting - Layout - Header */
	$wp_customize->add_setting( 'logo_alignment_pro', array(
		'default' => 'logo-left-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control('logo_alignment_pro', array(
		'label'    => __( 'Logo Alignment', 'progression' ),
		'section'  => 'section_header_layout_pro',
		'type' => 'select',
		'priority'   => 40,
		'choices'     => array(
			'logo-left-pro' => __( 'Left', 'progression' ),
			'logo-right-pro' => __( 'Right', 'progression' ),
		),
	)  );
	
	
	
	
	
	
	/* Section - Layout - Sidebars */
	$wp_customize->add_section( 'section_sidebar_pro', array(
		'title'          => __( 'Sidebar', 'progression' ),
		'panel'          => 'layout_panel_pro', // Not typically needed.
		'priority'       => 50,
	) );
	/* Setting - Layout - Sidebars */
	$wp_customize->add_setting( 'sidebar_position_pro', array(
		'default' => 'right-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'sidebar_position_pro', array(
		'label'    => __( 'Sidebar Position', 'progression' ),
		'section'  => 'section_sidebar_pro',
		'priority'   => 10,
		'choices'     => array(
			'left-pro' => __( 'Left', 'progression' ),
			'right-pro' => __( 'Right', 'progression' ),
		),
	) ) );
	/* Setting - Layout - Sidebars */
	$wp_customize->add_setting( 'sidebar_blog_check_pro', array(
		'default' => 'on-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'sidebar_blog_check_pro', array(
		'label'    => __( 'Display Sidebar on Blog', 'progression' ),
		'section'  => 'section_sidebar_pro',
		'priority'   => 20,
		'choices'     => array(
			'on-pro' => __( 'On', 'progression' ),
			'off-pro' => __( 'Off', 'progression' ),
		),
	) ) );
	
	/* Setting - Layout - Sidebars */
	$wp_customize->add_setting( 'sidebar_shop_check_pro', array(
		'default' => 'on-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'sidebar_shop_check_pro', array(
		'label'    => __( 'Display Sidebar on Shop', 'progression' ),
		'section'  => 'section_sidebar_pro',
		'priority'   => 20,
		'choices'     => array(
			'on-pro' => __( 'On', 'progression' ),
			'off-pro' => __( 'Off', 'progression' ),
		),
	) ) );
	
	
	
	
	
	
	
	
	/* Section - Layout - WooCommerce */
	$wp_customize->add_section( 'section_woocommerce_pro', array(
		'title'          => __( 'Shop', 'progression' ),
		'panel'          => 'layout_panel_pro', // Not typically needed.
		'priority'       => 85,
	) );
	/* Setting - Layout - WooCommerce */
	$wp_customize->add_setting( 'woocommerce_post_count_pro' ,array(
		'default' => '12',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( 'woocommerce_post_count_pro', array(
		'label'          => __( 'Shop Posts Per Page', 'progression' ),
		'section' => 'section_woocommerce_pro',
		'type' => 'text',
		'priority'   => 10,
		)
	);
	/* Setting - Layout - WooCommerce */
	$wp_customize->add_setting( 'woocommerce_columns_pro' ,array(
		'default' => '3',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control( 'woocommerce_columns_pro', array(
		'label'          => __( 'Shop Posts Per Row', 'progression' ),
		'section' => 'section_woocommerce_pro',
		'type' => 'select',
		'priority'   => 25,
		'choices' => array(
		            '1' => __( '1 Column', 'progression' ),
		            '2' => __( '2 Column', 'progression' ),
		            '3' => __( '3 Column', 'progression' ),
		            '4' => __( '4 Column', 'progression' ),
		 ),
		)
	);
	/* WooCommerce Related Products */
	$wp_customize->add_setting( 'pro_wc_related', array(
		'default' => 'related-off-pro',
		'sanitize_callback' => 'progression_sanitize_text',
	) );
	$wp_customize->add_control(new Progression_Controls_Radio_Buttonset_Control($wp_customize, 'pro_wc_related', array(
		'label'    => __( 'Show Related Products', 'progression' ),
		'section'  => 'section_woocommerce_pro',
		'priority'   => 26,
		'choices'     => array(
			'related-on-pro' => __( 'On', 'progression' ),
			'related-off-pro' => __( 'Off', 'progression' ),
		),
	) ) );	
	
	
	

	


}
add_action( 'customize_register', 'progression_customizer' );

/* Sanitize Text */
function progression_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}





function progression_customize_js()
{
    ?>
	<?php if (get_theme_mod( 'fixed_menu_pro') == "fixed-pro") : ?>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		'use strict';
		var $window = $(window);
		var nav = $('#sticky-pro');
		$window.scroll(function(){
		    if ($window.scrollTop() >= 1) {
		       nav.addClass('fixed-header');
		    }
		    else {
		       nav.removeClass('fixed-header');
		    }
		});
	}); 
	</script>
	<?php endif; ?>
    <?php
}
add_action('wp_footer', 'progression_customize_js');


function progression_customize_css()
{
    ?>


<?php if (get_theme_mod( 'pro_theme_fav_icon' )) : ?><link rel="icon"  href="<?php echo esc_attr(get_theme_mod( 'pro_theme_fav_icon')); ?>"><?php endif; ?>


<style type="text/css">

	/* Theme Settings */
	<?php if (get_theme_mod( 'fixed_menu_pro') == "fixed-pro") : ?>
	body.admin-bar #sticky-pro { margin-top:32px; }
	header#masthead-pro {-webkit-text-stroke: 0.5px; }
	#sticky-pro { position:fixed; }
	#sticky-pro.fixed-header { background:<?php echo esc_attr(get_theme_mod('sticky_header_bg_color_pro', '#253034')); ?>; }
	header#masthead-pro.slider-rev-logo #sticky-pro {position:fixed;}
	<?php endif; ?>
	
	header#masthead-pro #fixed-header-z { background: <?php echo esc_attr(get_theme_mod('header_bg_color_pro')); ?>; } 
	body #logo-pro, body #logo-pro img {max-width:<?php echo esc_attr(get_theme_mod('pro_theme_logo_width', '250')); ?>px;}
	header#masthead-pro h1#logo-pro { padding-top:<?php echo esc_attr(get_theme_mod('theme_logo_margin_top', '26')); ?>px; padding-bottom:<?php echo esc_attr(get_theme_mod('theme_logo_margin_bottom', '30')); ?>px; }
	<?php if (get_theme_mod( 'custom_css_pro')) : ?><?php echo esc_attr(get_theme_mod('custom_css_pro')); ?><?php endif; ?>
	<?php if (get_theme_mod( 'pro_global_comments') == "comment-off-pro") : ?>body.page #comments {display:none;}<?php endif; ?>
	<?php if (get_theme_mod( 'pro_scroll_top') == "scroll-off-pro") : ?>a#pro-scroll-top {display:none !important;}<?php endif; ?>
	<?php if (get_theme_mod( 'sidebar_position_pro') == "left-pro") : ?>#sidebar {float:left; } #main-container-pro {float:right;}<?php endif; ?>
	<?php if (get_theme_mod( 'pro_footer_align') == "footer-left-pro") : ?>#copyright-text-pro { text-align:left; }<?php endif; ?>
	<?php if (get_theme_mod( 'pro_footer_align') == "footer-right-pro") : ?>#copyright-text-pro { text-align:right; }<?php endif; ?>
	<?php if (get_theme_mod( 'pro_footer_widget_align') == "footer-center-pro") : ?>#widget-area-pro { text-align:center; }<?php endif; ?>
	<?php if (get_theme_mod( 'pro_footer_widget_align') == "footer-right-pro") : ?>#widget-area-pro { text-align:right; }<?php endif; ?>
	<?php if (get_theme_mod( 'sidebar_shop_check_pro') == "off-pro") : ?>body.woocommerce-page #main-container-pro {width:100%;} body.woocommerce-page #sidebar {display:none;}<?php endif; ?>
	<?php if (get_theme_mod( 'sidebar_blog_check_pro') == "off-pro") : ?>body.blog #main-container-pro, body.single-post #main-container-pro, body.search-results #main-container-pro, body.archive.category #main-container-pro {width:100%;} body.blog #sidebar, body.single-post #sidebar, body.search-results #sidebar, body.archive.category #sidebar {display:none;}<?php endif; ?>
	<?php if (get_theme_mod( 'pro_wc_related') == "related-off-pro") : ?>body .related.products {display: none;}<?php endif; ?>	
	
	/* Theme Colors */
	footer#site-footer #pro-scroll-top { color:<?php echo esc_attr(get_theme_mod('pro_scroll_color', '#ffffff')); ?>; background: <?php echo esc_attr(get_theme_mod('pro_scroll_bg_color', '#4abd92')); ?>; border-top:1px solid <?php echo esc_attr(get_theme_mod('pro_scroll_border_color', 'rgba(255,255,255,  0.2)')); ?>; border-left:1px solid <?php echo esc_attr(get_theme_mod('pro_scroll_border_color', 'rgba(255,255,255,  0.2)')); ?>; border-right:1px solid <?php echo esc_attr(get_theme_mod('pro_scroll_border_color', 'rgba(255,255,255,  0.2)')); ?>; }
	footer#site-footer #pro-scroll-top:hover {  color:<?php echo esc_attr(get_theme_mod('pro_scroll_hvr_color', '#ffffff')); ?>; background: <?php echo esc_attr(get_theme_mod('pro_scroll_hvr_bg_color', '#4abd92')); ?>; border-color:<?php echo esc_attr(get_theme_mod('pro_scroll_hvr_border_color', 'rgba(255,255,255,  0)')); ?>; }
	.sf-mega, .sf-menu ul, ul.mobile-menu-pro li ul li {  background:<?php echo esc_attr(get_theme_mod('sub_nav_bg_color_pro', 'rgba(38,42,48,  0.99)')); ?>; }
	.sf-menu ul, ul.mobile-menu-pro ul { border-top:2px solid <?php echo esc_attr(get_theme_mod('sub_navborder_pro', '#4abd92')); ?>; }
	#sidebar { background:<?php echo esc_attr(get_theme_mod('sidebar_bg_pro', '#deded4')); ?>; }
	.product-container-pro, body.single-product  .single-product-container-pro {background:<?php echo esc_attr(get_theme_mod('shop_container_bg_pro', '#deded4')); ?>; border:1px solid <?php echo esc_attr(get_theme_mod('shop_container_bg_pro', '#deded4')); ?>; }
	#content-pro  .width-container-pro span.onsale { background:<?php echo esc_attr(get_theme_mod('highlight_color_pro', '#4abd92')); ?>; }
	body ul.page-numbers span, body ul.page-numbers a { background:<?php echo esc_attr(get_theme_mod('highlight_color_pro', '#4abd92')); ?>; }
	body ul.page-numbers span.current, body ul.page-numbers a:hover, .page-links-pro a:hover { background:<?php echo esc_attr(get_theme_mod('sec_highlight_color_pro', '#253034')); ?>; border-color:<?php echo esc_attr(get_theme_mod('sec_highlight_color_pro', '#253034')); ?>;}
	body #content-pro #events-full-calendar .fc-toolbar { background:<?php echo esc_attr(get_theme_mod('highlight_color_pro', '#4abd92')); ?>; border-color:<?php echo esc_attr(get_theme_mod('highlight_color_pro', '#4abd92')); ?>;}
	.menu-collapser, ul.mobile-menu-pro, .collapse-button, ul.mobile-menu-pro li {background-color:<?php echo esc_attr(get_theme_mod('mobi_nav_bg_color_pro', 'rgba(24,26,30,0.99)')); ?>;}
	nav#site-navigation {background-color:<?php echo esc_attr(get_theme_mod('nav_bg_color_pro', 'rgba(24,26,30,0)')); ?>;}	
	#widget-area-pro a i {color:#253034;}	
	header#masthead-pro {background-color:<?php echo esc_attr(get_theme_mod('title_bg_color_pro', '#181a1e')); ?>;}
	body {background-color:<?php echo esc_attr(get_theme_mod('body_bg_color_pro', '#ffffff')); ?>;}
	#boxed-layout-pro {background-color:<?php echo esc_attr(get_theme_mod('boxed_bg_color_pro', '#ffffff')); ?>;}	
	footer#site-footer {background-color:<?php echo esc_attr(get_theme_mod('footer_bg_color_pro', '#253034')); ?>;}

	/* Background Images */
	<?php if (get_theme_mod( 'nav_bg_image_pro')) : ?>		
		nav#site-navigation { background-image:url(<?php echo esc_attr(get_theme_mod( 'nav_bg_image_pro')); ?>) ; <?php if (get_theme_mod( 'nav_bg_cover_pro') == "pattern-pro") : ?>background-repeat:repeat;<?php endif; ?><?php if (get_theme_mod( 'nav_bg_cover_pro') == "cover-pro") : ?>background-size:cover; background-position: center center; background-repeat: no-repeat;<?php endif; ?>}		
	<?php endif; ?>		
	<?php if (get_theme_mod( 'title_bg_image_pro')) : ?>
		<?php if (get_theme_mod( 'title_bg_cover_pro') == "pattern-pro") : ?>
				header#masthead-pro { background-image:url(<?php echo esc_attr(get_theme_mod( 'title_bg_image_pro')); ?>) ; background-repeat:repeat;}
		<?php endif; ?>
	<?php endif; ?>		
	<?php if (get_theme_mod( 'body_bg_image_pro')) : ?>
		body { background-image:url(<?php echo esc_attr(get_theme_mod( 'body_bg_image_pro')); ?>) ; <?php if (get_theme_mod( 'body_bg_cover_pro') == "pattern-pro") : ?>background-repeat:repeat;<?php endif; ?><?php if (get_theme_mod( 'body_bg_cover_pro', 'cover-pro') == "cover-pro") : ?>background-size:cover; background-position: center center; background-repeat: no-repeat;<?php endif; ?>}		
	<?php endif; ?>		
	<?php if (get_theme_mod( 'boxed_bg_image_pro')) : ?>
		#boxed-layout-pro { background-image:url(<?php echo esc_attr(get_theme_mod( 'boxed_bg_image_pro')); ?>) ; <?php if (get_theme_mod( 'boxed_bg_cover_pro') == "pattern-pro") : ?>background-repeat:repeat;<?php endif; ?><?php if (get_theme_mod( 'boxed_bg_cover_pro', 'cover-pro') == "cover-pro") : ?>background-size:cover; background-position: center center; background-repeat: no-repeat;<?php endif; ?>}		
	<?php endif; ?>	
	<?php if (get_theme_mod( 'footer_bg_image_pro')) : ?>		
		footer#site-footer { background-image:url(<?php echo esc_attr(get_theme_mod( 'footer_bg_image_pro')); ?>) ; <?php if (get_theme_mod( 'footer_bg_cover_pro') == "pattern-pro") : ?>background-repeat:repeat;<?php endif; ?><?php if (get_theme_mod( 'footer_bg_cover_pro', 'cover-pro') == "cover-pro") : ?>background-size:cover; background-position: center center; background-repeat: no-repeat;<?php endif; ?>}		
	<?php endif; ?>

	/* Page Title Backgrounds Conditionals */	
	<?php global $post; if(is_page() && get_post_meta($post->ID, 'progression_slider', true) ): ?>
		<?php else: ?>
		<?php if(is_page() && has_post_thumbnail() ): ?>
			<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>	
				header#masthead-pro {background-image: url("<?php echo esc_attr($image[0]);?>"); background-size:cover; background-position: center center; background-repeat: no-repeat;}
		<?php else: ?>
			<?php if(is_home()): ?>
				<?php $page_for_posts2 = get_option( 'page_for_posts' ); if(wp_get_attachment_image_src(get_post_thumbnail_id($page_for_posts2))): 	?>
					<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($page_for_posts2), 'full'); ?>
						header#masthead-pro {background-image: url("<?php echo esc_attr($image[0]);?>"); background-size:cover; background-position: center center; background-repeat: no-repeat;}
				<?php else: ?>
					<?php if (get_theme_mod( 'title_bg_cover_pro', 'cover-pro') == "cover-pro") : ?>
						header#masthead-pro {background-image: url("<?php echo esc_attr(get_theme_mod( 'title_bg_image_pro', get_template_directory_uri() . '/images/page-title.jpg' )); ?>"); background-size:cover; background-position: center center; background-repeat: no-repeat;}
					<?php endif; ?>
				<?php endif; ?>
			<?php else: ?>
				<?php if (get_theme_mod( 'title_bg_image_pro',  get_template_directory_uri() . '/images/page-title.jpg' )) : ?>
					<?php if (get_theme_mod( 'title_bg_cover_pro', 'cover-pro') == "cover-pro") : ?>
						header#masthead-pro {background-image: url("<?php echo esc_attr(get_theme_mod( 'title_bg_image_pro', get_template_directory_uri() . '/images/page-title.jpg' )); ?>"); background-size:cover; background-position: center center; background-repeat: no-repeat;}
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>	
	<?php endif; ?>	
	
	
	 <?php if (get_theme_mod( 'logo_alignment_pro') == "logo-right-pro") : ?>header#masthead-pro h1#logo-pro { float:right;}<?php endif; ?>
	<?php if (get_theme_mod( 'nav_alignment_pro') == "nav-left-pro") : ?>nav#site-navigation { float:left;  }<?php endif; ?>
</style>


    <?php
}
add_action('wp_head', 'progression_customize_css');