<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package progression
 */

get_header(); ?>

	<div id="page-header-pro"><div class="width-container-pro"><h1 class="entry-title-pro"><?php _e( 'Oops! That page can&rsquo;t be found.', 'progression' ); ?></h1></div></div><!-- #page-header-pro -->
	</header><!-- #masthead-pro -->
	
	
	<div id="content-pro" class="site-content">
		<div class="width-container-pro">
			
			<br>
			<p><?php _e( 'It looks like nothing was found at this location. ', 'progression' ); ?></p>
			<br>
			
		<div class="clearfix-pro"></div>
		</div><!-- close .width-container-pro -->
	</div><!-- #content-pro -->
<?php get_footer(); ?>