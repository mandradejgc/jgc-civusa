<?php
/**
 * The template for displaying search results pages.
 *
 * @package progression
 */

get_header(); ?>

	<div id="page-header-pro"><div class="width-container-pro"><h1 class="entry-title-pro"><?php printf( esc_html__( 'Search Results for: %s', 'progression' ), '<span>' . get_search_query() . '</span>' ); ?></h1></div></div><!-- #page-header-pro -->
	</header><!-- #masthead-pro -->

	<div id="content-pro" class="site-content">
		<div class="width-container-pro">
			
			<div id="main-container-pro">

				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'template-parts/content', 'search' ); ?>
					<?php endwhile; ?>
					<?php show_pagination_links_pro( ); ?>
				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>
			</div><!-- close #main-container-pro -->
			
			<?php get_sidebar(); ?>
			
		<div class="clearfix-pro"></div>
		</div><!-- close .width-container-pro -->
	</div><!-- #content-pro -->
<?php get_footer(); ?>