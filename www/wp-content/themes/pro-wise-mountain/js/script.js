/*  Table of Contents 
01. MENU ACTIVATION
02. FITVIDES RESPONSIVE VIDEOS
03. SCROLL TO TOP BUTTON
04. STICKY HEADER
05. Show/Hide Navigation Mobile/Tablet
06. GALLERY JS
07. PRETTYPHOTO BLOG SETTINGS
08. SLIMMENU MOBILE MENU
*/

jQuery(document).ready(function($) {
	 'use strict';
	 
/*
=============================================== 01. MENU ACTIVATION  ===============================================
*/
	 jQuery('ul.sf-menu').superfish({
			 	popUpSelector: 'ul.sub-menu,.sf-mega', 	// within menu context
	 			delay:      	700,                	// one second delay on mouseout
	 			speed:      	200,               		// faster animation speed
		 		speedOut:    	200,             		// speed of the closing animation
				animation: 		{opacity: 'show'},		// animation out
				animationOut: 	{opacity: 'hide'},		// adnimation in
		 		cssArrows:     	true,              		// set to false
			 	autoArrows:  	true                    // disable generation of arrow mark-up
	 });

/*
=============================================== 02. FITVIDES RESPONSIVE VIDEOS  ===============================================
*/
	
	$("#content-pro").fitVids();	 		

/*
=============================================== 03. SCROLL TO TOP BUTTON  ===============================================
*/
  	// browser window scroll (in pixels) after which the "back to top" link is shown
  	var offset = 150,
  	
	//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
  	offset_opacity = 1200,
  	
	//duration of the top scrolling animation (in ms)
  	scroll_top_duration = 700,
  	
	//grab the "back to top" link
  	$back_to_top = $('#pro-scroll-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
  		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
  		if( $(this).scrollTop() > offset_opacity ) { 
  			$back_to_top.addClass('cd-fade-out');
  		}
  	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		}, scroll_top_duration
	);
	});

/*
=============================================== 04. STICKY HEADER  ===============================================
*/
    $('#sticky-header-pro').scrollToFixed({
		spacerClass: 'spacer-fix-pro'
    });

/*
=============================================== 05. Show/Hide Navigation Mobile/Tablet  ===============================================
*/
 	$(".menu-show-hide-pro").click(function(){
		$(".menu-main-navigation-container").toggleClass("toggle-nav-pro"); 
		$(".menu-show-hide-pro").toggleClass("toggle-button-pro"); 
 	});


/*
=============================================== 06. GALLERY JS  ===============================================
*/	
    $('.gallery-progression').flexslider({
		animation: "fade",      
		slideDirection: "horizontal", 
		slideshow: false,         
		slideshowSpeed: 7000,  
		animationDuration: 200,        
		directionNav: true,             
		controlNav: true,
		prevText: "",    
		nextText: "", 
    });
/*
=============================================== 07. PRETTYPHOTO BLOG SETTINGS  ===============================================
*/		

	$(".featured-blog-pro a[data-rel^='prettyPhoto'], .pro-media-image a[data-rel^='prettyPhoto'], .images a[data-rel^='prettyPhoto'], .single-wine a[data-rel^='prettyPhoto'], .single-event a[data-rel^='prettyPhoto']").prettyPhoto({
			hook: 'data-rel',
				animation_speed: 'fast', /* fast/slow/normal */
				slideshow: 5000, /* false OR interval time in ms */
				show_title: false, /* true/false */
				deeplinking: false, /* Allow prettyPhoto to update the url to enable deeplinking. */
				overlay_gallery: false, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
				custom_markup: '',
				social_tools: '' /* html or false to disable */
	});	


/*
=============================================== 08. SLIMMENU MOBILE MENU  ===============================================
*/	
	$('ul.mobile-menu-pro').slimmenu({
	    resizeWidth: '959',
	    collapserTitle: 'Menu',
	    easingEffect:'easeInOutQuint',
	    animSpeed:'medium',
	    indentChildren: false,
		childrenIndenter: '- '
	});
	
	
	
});