<?php
/**
 * The template for displaying all single posts.
 *
 * @package progression
 */

get_header(); ?>

	<div id="page-header-pro"><div class="width-container-pro"><h1 class="entry-title-pro"><?php the_title(); ?></h1></div></div><!-- #page-header-pro -->
	</header><!-- #masthead-pro -->

	<div id="content-pro" class="site-content">
		<div class="width-container-pro">
			
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'single-wine' ); ?>
			
				<?php endwhile; // end of the loop. ?>
			
			
		<div class="clearfix-pro"></div>
		</div><!-- close .width-container-pro -->
	</div><!-- #content-pro -->
<?php get_footer(); ?>