<?php if (get_theme_mod( 'show_social_pro', '1' )) : ?>
<div id="header-right-pro">
<div class="social-ico">
	<?php if (get_theme_mod( 'footer_facebook_link_progression', 'http://facebook.com' )) : ?><a href="<?php echo esc_url(get_theme_mod('facebook_link_progression', 'http://facebook.com')); ?>" target="_blank"><i class="fa fa-facebook"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'footer_twitter_link_progression', 'http://twitter.com' )) : ?><a href="<?php echo esc_url(get_theme_mod('twitter_link_progression', 'http://twitter.com')); ?>" target="_blank"><i class="fa fa-twitter"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'linkedin_link_progression', 'http://linkedin.com' )) : ?><a href="<?php echo esc_url(get_theme_mod('linkedin_link_progression', 'http://linkedin.com')); ?>" target="_blank"><i class="fa fa-linkedin"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'instagram_link_progression', 'http://instagram.com' )) : ?><a href="<?php echo esc_url(get_theme_mod('instagram_link_progression', 'http://instagram.com')); ?>" target="_blank"><i class="fa fa-instagram"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'footer_pinterest_link_progression', 'http://pinterest.com' )) : ?><a href="<?php echo esc_url(get_theme_mod('pinterest_link_progression', 'http://pinterest.com')); ?>" target="_blank"><i class="fa fa-pinterest"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'tumblr_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('tumblr_link_progression')); ?>" target="_blank"><i class="fa fa-tumblr"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'youtube_link_progression', 'http://youtube.com' )) : ?><a href="<?php echo esc_url(get_theme_mod('youtube_link_progression', 'http://youtube.com')); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'vimeo_link_progression', 'http://vimeo.com' )) : ?><a href="<?php echo esc_url(get_theme_mod('vimeo_link_progression', 'http://vimeo.com')); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'dropbox_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('dropbox_link_progression')); ?>" target="_blank"><i class="fa fa-dropbox"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'flickr_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('flickr_link_progression')); ?>" target="_blank"><i class="fa fa-flickr"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'dribbble_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('dribbble_link_progression')); ?>" target="_blank"><i class="fa fa-dribbble"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'footer_google_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('google_link_progression')); ?>" target="_blank"><i class="fa fa-google-plus"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'vine_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('vine_link_progression')); ?>" target="_blank"><i class="fa fa-vine"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'soundcloud_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('soundcloud_link_progression')); ?>" target="_blank"><i class="fa fa-soundcloud"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'spotify_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('spotify_link_progression')); ?>" target="_blank"><i class="fa fa-spotify"></i></a><?php endif; ?>
	<?php if (get_theme_mod( 'footer_mail_link_progression' )) : ?><a href="<?php echo esc_url(get_theme_mod('mail_link_progression')); ?>" target="_blank"><i class="fa fa-envelope"></i></a><?php endif; ?>
</div>
</div>
<?php endif; ?>