<?php
/**
 * progression functions and definitions
 *
 * @package progression
 * @since progression 1.0
 */


if ( ! function_exists( 'progression_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * @since progression 1.0
 */

	
function progression_setup() {

	if(function_exists( 'set_revslider_as_theme' )){
		add_action( 'init', 'pro_ezio_custom_slider_rev' );
		function pro_ezio_custom_slider_rev() { set_revslider_as_theme(); }
	}
	add_action( 'vc_before_init', 'progression_SetAsTheme' );
	function progression_SetAsTheme() { vc_set_as_theme( $disable_updater = true ); }

	
	
	// Post Thumbnails
	add_theme_support( 'post-thumbnails' );
	add_image_size('progression-blog', 800, 350, true); // Blog Post Image
	add_image_size('progression-wine', 380, 830, false); // Wine Post Image
	add_image_size('progression-event', 600, 350, true); // Wine Post Image

	
	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on progression, use a find and replace
	 * to change 'progression' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'progression', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );
	
	/**
	 * Custom Widgets
	 */
	require( get_template_directory() . '/inc/widgets/widgets.php' );
	
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );
	
	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio', 'link' ) );
	
	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'progression' ),
	) );
	
}
endif; // progression_setup
add_action( 'after_setup_theme', 'progression_setup' );
add_filter( 'got_rewrite', '__return_true', 999 );

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since progression 1.0
 */
if ( ! isset( $content_width ) )
	$content_width = 1200; /* pixels */


/**
 * Register widgetized area and update sidebar with default widgets
 *
 * @since progression 1.0
 */
function progression_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'progression' ),
		'description'   => 'Default sidebar',
		'id' => 'sidebar-1',
		'before_widget' => '<div id="%1$s" class="sidebar-item widget %2$s">',
		'after_widget' => '</div><div class="sidebar-divider-pro"></div>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );

	
	register_sidebar( array(
		'name' => __( 'Footer Widgets', 'progression' ),
		'description' => __( 'Footer widgets', 'progression' ),
		'id' => 'footer-widgets',
		'before_widget' => '<div id="%1$s" class="widget-pro %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h6 class="widget-title">',
		'after_title' => '</h6>',
	) );

	
	register_sidebar( array(
		'name' => __( 'Shop Sidebar', 'progression' ),
		'description'   => 'WooCommerce sidebar',
		'id' => 'sidebar-shop',
		'before_widget' => '<div id="%1$s" class="sidebar-item widget %2$s">',
		'after_widget' => '</div><div class="sidebar-divider-pro"></div>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
}
add_action( 'widgets_init', 'progression_widgets_init' );



function progression_studios_google_maps_customizer( $wp_customize ) {
	$wp_customize->add_section( 'progression_studios_panel_google_Maps', array(
		'priority'    => 800,
       'title'       => esc_html__( 'Google Maps', 'invested-progression' ),
    ) );
	 
	$wp_customize->add_setting( 'progression_studios_google_maps_api' ,array(
		'default' =>  '',
		'sanitize_callback' => 'progression_studios_google_maps_sanitize_text',
	) );
	$wp_customize->add_control( 'progression_studios_google_maps_api', array(
		'label'          => 'Google Maps API Key',
		'description'    => 'See documentation under "Google Maps API Key" for directions. Get API key: https://developers.google.com/maps/documentation/javascript/get-api-key',
		'section' => 'progression_studios_panel_google_Maps',
		'type' => 'text',
		'priority'   => 10,
		)
	
	);
}
add_action( 'customize_register', 'progression_studios_google_maps_customizer' );
/* Sanitize Text */
function progression_studios_google_maps_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}



/**
 * Enqueue scripts and styles
 */
function progression_scripts() {
	
   if ( get_theme_mod( 'progression_studios_google_maps_api') ) {
       $progression_studios_google_api_url =  '?key='. get_theme_mod('progression_studios_google_maps_api');
	} else {
		$progression_studios_google_api_url = '';
	}
	
	wp_enqueue_style( 'progression-style', get_stylesheet_uri() );
	wp_enqueue_style( 'google-fonts', 'http://fonts.googleapis.com/css?family=Crimson+Text:400,400italic,600,700|Lato:100,300,400,700', array( 'progression-style' ) );
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array( 'jquery' ), '20120206', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '20120206', true );
	wp_enqueue_script( 'google_maps', 'https://maps.google.com/maps/api/js' . $progression_studios_google_api_url, 1 );
	wp_enqueue_script( 'gomap', get_template_directory_uri() . '/js/jquery.gomap-1.3.3.min.js', array( 'jquery' ), '20120206', false );

	wp_enqueue_style( 'prettyphoto' );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
	
}
add_action( 'wp_enqueue_scripts', 'progression_scripts' );


/**
 * Remove plugin scripts and styles
 */
function pro_deregisterstyles() {
	wp_deregister_style( 'flexslider' );
	wp_deregister_style( 'font-awesome' );
	wp_deregister_script( 'prettyphoto' );
}
add_action( 'wp_enqueue_scripts', 'pro_deregisterstyles', 100 );


/**
 * Custom Editor Styles
 */
function progression_add_editor_styles() {
    add_editor_style( 'inc/editor-style.css' );
}
add_action( 'admin_init', 'progression_add_editor_styles' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom Metabox Fields
 */
require get_template_directory() . '/inc/Custom-Meta-Boxes/custom-meta-boxes.php';

/**
 * Theme Customizer
 */
require get_template_directory() . '/inc/default-customizer.php';

/**
 * Theme Customizer
 */
require get_template_directory() . '/inc/mega-menu/mega-menu-framework.php';


/**
 * WooCommerce Functions
 */
if ( ! function_exists( 'woocommerce' ) ) require get_template_directory() . '/inc/woocommerce-functions.php';

/**
 * Load Plugin Activiation
 */
require get_template_directory() . '/inc/tgm-plugin-activation/plugin-activation.php';
