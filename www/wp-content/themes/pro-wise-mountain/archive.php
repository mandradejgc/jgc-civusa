<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package progression
 */

get_header(); ?>

	<div id="page-header-pro"><div class="width-container-pro"><?php the_archive_title( '<h1 class="entry-title-pro">', '</h1>' ); ?> </div></div><!-- #page-header-pro -->
	</header><!-- #masthead-pro -->

	<div id="content-pro" class="site-content">
		<div class="width-container-pro">
			
			<div id="main-container-pro">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'template-parts/content', get_post_format() );	?>
					<?php endwhile; ?>
					<?php show_pagination_links_pro( ); ?>
				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>
			</div><!-- close #main-container-pro -->
			
			<?php get_sidebar(); ?>
			
		<div class="clearfix-pro"></div>
		</div><!-- close .width-container-pro -->
	</div><!-- #content-pro -->
<?php get_footer(); ?>