<?php
/**
 * The Header for our theme.
 *
 * @package progression
 * @since progression 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	
	<?php if (get_theme_mod( 'pro_site_layout_wide' ) == 'boxed-pro') : ?><div id="boxed-layout-pro"><?php endif; ?>
	
	<div id="mobile-menu-container">	
	<?php wp_nav_menu( array('theme_location' => 'primary', 'menu_class' => 'mobile-menu-pro', 'fallback_cb' => false, 'walker'  => new ProgressionFrontendWalker ) ); ?>
	</div><!-- close #mobile-menu-container -->
	
	<header id="masthead-pro" class="site-header-pro<?php global $post; if(is_page() && get_post_meta($post->ID, 'progression_slider', true) ): ?> slider-rev-logo<?php endif; ?><?php if(is_home()): ?><?php if( get_option( 'page_for_posts' ) ) : $cover_page = get_page( get_option( 'page_for_posts' ) ); ?><?php if(get_post_meta($cover_page->ID, 'progression_slider', true)): ?> slider-rev-logo<?php endif ?><?php endif ?><?php endif ?>">
		
		<div id="sticky-pro">
			
		<div id="fixed-header-z">
		<div class="width-container-pro ">
		
		<?php if (get_theme_mod( 'pro_theme_logo', get_template_directory_uri() . '/images/logo.png' )) : ?><h1 id="logo-pro"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo esc_attr(get_theme_mod( 'pro_theme_logo', get_template_directory_uri() . '/images/logo.png' )); ?>" alt="<?php bloginfo('name'); ?>" width="<?php echo esc_attr(get_theme_mod( 'pro_theme_logo_width', '250' )); ?>" /></a></h1><?php endif; ?>
		
		<nav id="site-navigation" class="main-navigation">
				<?php wp_nav_menu( array('theme_location' => 'primary', 'menu_class' => 'sf-menu', 'fallback_cb' => false, 'walker'  => new ProgressionFrontendWalker ) ); ?><?php if ( has_nav_menu( 'primary' ) ):  ?><?php else: ?><span class="nav-pro-span"><?php esc_html_e( 'Insert Navigation under Appearance > Menus', 'progression' ); ?></span><?php endif; ?>
			<div class="clearfix-pro"></div>
		</nav>
		</div><!-- close .width-container-pro -->
		</div>
		</div><!-- close #sticky-pro -->
		
		<?php  global $post; if(is_page() && get_post_meta($post->ID, 'progression_slider', true) ): ?><div id="pro-home-slider"><?php echo apply_filters('the_content', get_post_meta($post->ID, 'progression_slider', true)); ?></div><?php endif; ?>