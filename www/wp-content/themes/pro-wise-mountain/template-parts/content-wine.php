<?php
/**
 * @package progression
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="wine-post-pro">			
			<?php if(has_post_thumbnail()): ?>				
				<?php if(get_post_meta($post->ID, 'pro_wine_price', true)): ?><div class="wine-price-pro"><?php echo get_post_meta($post->ID, 'pro_wine_price', true) ?></div><?php endif; ?>
				<div class="wine-thumb-pro"><?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php else: ?><a href="<?php the_permalink();?>"><?php endif; ?><?php the_post_thumbnail('progression-wine'); ?><?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?></a><?php else: ?></a><?php endif;?></div>				
				<div class="wine-thumb-container">
			<?php else: ?>
				<?php if(get_post_meta($post->ID, 'pro_gallery', true) ): ?>					
					<div class="wine-thumb-pro">
						<div class="flexslider gallery-progression">
				      		<ul class="slides">
								<?php $gallery_pro = get_post_meta( get_the_id(), 'pro_gallery', false ); foreach ( $gallery_pro as $single_gallery_img ) ?>
								<?php if($gallery_pro):  foreach($gallery_pro as $single_gallery_img): ?>
									<?php $image = wp_get_attachment_image_src($single_gallery_img, 'progression-wine'); ?>				
									<li>
										<?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php endif; ?><img src="<?php echo esc_attr($image[0]);?>" alt="Gallery Image"></a>
									</li>
								<?php endforeach; endif; ?>
							</ul>
						</div><!-- close .flexslider -->
					</div>					
					<div class="wine-thumb-container">
				<?php else: ?>
					<?php if(get_post_meta($post->ID, 'pro_video_post', true)): ?>
						<div class="wine-thumb-pro">
							<?php echo apply_filters('the_content', get_post_meta($post->ID, 'pro_video_post', true)); ?>
						</div>
						<div class="wine-thumb-container">
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
			
			
				<h6 class="wine-title-pro"><?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php else: ?><a href="<?php the_permalink();?>"><?php endif; ?><?php the_title(); ?></a></h6>	
				<?php if( has_excerpt() ): ?><div class="wine-excerpt-pro"><?php the_excerpt(); ?></div><?php else: ?><div class="wine-excerpt-pro"><?php the_content(); ?></div><?php endif; ?>
				
				<?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>" class="pro-button-shortcode default-style-pro normal-size-pro"><?php else: ?><a href="<?php the_permalink();?>" class="pro-button-shortcode default-style-pro normal-size-pro "><?php endif; ?><?php _e( 'Read More', 'progression' ); ?></a>
			
			<?php if(has_post_thumbnail()): ?></div><!-- close .wine-thumb-container --><?php else: ?><?php if(get_post_meta($post->ID, 'pro_gallery', true) ): ?></div><!-- close .wine-thumb-container --><?php else: ?><?php if(get_post_meta($post->ID, 'pro_video_post', true)): ?></div><!-- close .wine-thumb-container --><?php endif; ?>
				<?php endif; ?><?php endif; ?>
		
		<div class="clearfix-pro"></div>
	</div><!-- close .wine-container-pro -->
</article><!-- #post-## -->