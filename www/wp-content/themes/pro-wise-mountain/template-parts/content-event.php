<?php
/**
 * @package progression
 */
?>
<li class="event-list-item">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="event-post-pro">
			<div class="grid2column-progression img-event-progression">	
				<?php if(has_post_thumbnail()): ?>
					<div class="event-thumb-pro"><?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php else: ?><a href="<?php the_permalink();?>"><?php endif; ?><?php the_post_thumbnail('progression-event'); ?><?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?></a><?php else: ?></a><?php endif;?></div>
					<div class="event-thumb-container">
				<?php else: ?>
					<?php if(get_post_meta($post->ID, 'pro_gallery', true) ): ?>
						<div class="event-thumb-pro">
							<div class="flexslider gallery-progression">
								<ul class="slides">
									<?php $gallery_pro = get_post_meta( get_the_id(), 'pro_gallery', false ); foreach ( $gallery_pro as $single_gallery_img ) ?>
									<?php if($gallery_pro):  foreach($gallery_pro as $single_gallery_img): ?>
										<?php $image = wp_get_attachment_image_src($single_gallery_img, 'progression-event'); ?>				
										<li>
											<?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php endif; ?><img src="<?php echo esc_attr($image[0]);?>" alt="Gallery Image"></a>
										</li>
									<?php endforeach; endif; ?>
								</ul>
							</div><!-- close .flexslider -->
						</div>
						<div class="event-thumb-container">
					<?php else: ?>
						<?php if(get_post_meta($post->ID, 'pro_video_post', true)): ?>
							<div class="event-thumb-pro">
								<?php echo apply_filters('the_content', get_post_meta($post->ID, 'pro_video_post', true)); ?>
							</div>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>			
				<?php if(has_post_thumbnail()): ?></div><!-- close .event-thumb-container --><?php else: ?><?php if(get_post_meta($post->ID, 'pro_gallery', true) ): ?></div><!-- close .event-thumb-container --><?php else: ?><?php if(get_post_meta($post->ID, 'pro_video_post', true)): ?></div><!-- close .event-thumb-container --><?php endif; ?>
				<?php endif; ?><?php endif; ?>			
			</div>
				
			<div class="grid2column-progression content-event-progression">
				<h5 class="event-title-pro"><?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php else: ?><a href="<?php the_permalink();?>"><?php endif; ?><?php the_title(); ?></a></h5>	
					<?php do_action ('em_after_single_event_title');?>
				<?php if( has_excerpt() ): ?><div class="event-excerpt-pro"><?php the_excerpt(); ?></div><?php else: ?><div class="event-excerpt-pro"><?php the_content(); ?></div><?php endif; ?>
				
				<?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>" class="pro-button-shortcode default-style-pro normal-size-pro"><?php else: ?><a href="<?php the_permalink();?>" class="pro-button-shortcode event-btn default-style-pro small-size-pro "><?php endif; ?><?php _e( 'View Event', 'progression' ); ?></a>
			</div><div class="clearfix-pro"></div>
		</div><!-- close .event-container-pro --><div class="clearfix-pro"></div>
		<div class="clearfix-pro"></div>
	</article><!-- #post-## --><div class="clearfix-pro"></div>
</li>	