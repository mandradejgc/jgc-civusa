<?php
/**
 * @package progression
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="wine-post-container-pro">
		<?php if(get_post_meta($post->ID, 'pro_gallery', true) ): ?>
			<div class="wine-featured-blog-pro">
				<div class="flexslider gallery-progression">
		      		<ul class="slides gallery">
						<?php $gallery_pro = get_post_meta( get_the_id(), 'pro_gallery', false ); ?>
						<?php if($gallery_pro):  foreach($gallery_pro as $single_gallery_img): ?>
							<?php $image = wp_get_attachment_image_src($single_gallery_img, 'progression-wine'); ?>		
							<?php $thumbnail = wp_get_attachment_image_src($single_gallery_img, 'large'); ?>
							<li class="gallery-item">
								<a href="<?php echo esc_attr($thumbnail[0]);?>" data-rel="prettyPhoto[gallery]"><img src="<?php echo esc_attr($image[0]);?>" alt="Photo-<?php echo esc_attr($single_gallery_img); ?>"></a>
							</li>
						<?php endforeach; endif; ?>
					</ul>
				</div><!-- close .flexslider -->
			</div><!-- close .wine-featured-blog-pro -->
		<?php else: ?>
			
			<?php if(get_post_meta($post->ID, 'pro_video_post', true)): ?>
				<div class="wine-featured-blog-pro"><?php echo apply_filters('the_content', get_post_meta($post->ID, 'pro_video_post', true)); ?></div>
			<?php else: ?>
				<?php if(has_post_thumbnail()): ?>
					<div class="wine-featured-blog-pro"><?php if(get_post_meta($post->ID, 'pro_wine_price', true)): ?><div class="wine-price-pro"><?php echo get_post_meta($post->ID, 'pro_wine_price', true) ?></div><?php endif; ?><?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?><a href="<?php echo esc_attr($image[0]);?>" data-rel="prettyPhoto"><?php the_post_thumbnail('progression-wine'); ?></a></div>
				<?php endif; ?>
			<?php endif; ?>
	
		<?php endif; ?>

		<div class="wine-entry-content-pro">

				
			<div class="wine-summary-post-pro">
				<?php if(get_post_meta($post->ID, 'pro_wine_sub_header', true)): ?><div class="pro-sub-header"><?php echo get_post_meta($post->ID, 'pro_wine_sub_header', true) ?></div><?php endif; ?>
				<?php the_content(); ?>
			</div><!-- close .summary-post-pro -->
			
			<?php wp_link_pages( array(
					'before' => '<div class="page-nav-pro">' . esc_html__( 'Pages:', 'progression' ),
					'after'  => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
			?>
			
			
					
		</div><!-- .entry-content-pro -->
		
		<div class="clearfix-pro"></div>
	</div><!-- close .post-container-pro -->
</article><!-- #post-## -->
