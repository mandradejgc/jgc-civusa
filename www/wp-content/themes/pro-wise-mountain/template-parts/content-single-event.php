<?php
/**
 * The template for displaying event content in the single-event.php template
 *
 * Override this template by copying it to yourtheme/content-single-event.php
 *
 * @author 	Digital Factory
 * @package Events Maker/Templates
 * @since 	1.1.0
 */
 
if (!defined('ABSPATH')) exit; // Exit if accessed directly

// Extra event classes
$classes = apply_filters('em_loop_event_classes', array('hcalendar'));

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="event-post-container-pro">
		<?php if(get_post_meta($post->ID, 'pro_gallery', true) ): ?>
			<div class="event-featured-blog-pro grid2column-progression">
				<div class="flexslider gallery-progression">
		      		<ul class="slides gallery">
						<?php $gallery_pro = get_post_meta( get_the_id(), 'pro_gallery', false ); ?>
						<?php if($gallery_pro):  foreach($gallery_pro as $single_gallery_img): ?>
							<?php $image = wp_get_attachment_image_src($single_gallery_img, 'progression-event'); ?>		
							<?php $thumbnail = wp_get_attachment_image_src($single_gallery_img, 'large'); ?>
							<li class="gallery-item">
								<a href="<?php echo esc_attr($thumbnail[0]);?>" data-rel="prettyPhoto[gallery]"><img src="<?php echo esc_attr($image[0]);?>" alt="Photo-<?php echo esc_attr($single_gallery_img); ?>"></a>
							</li>
						<?php endforeach; endif; ?>
					</ul>
				</div><!-- close .flexslider -->
			</div><!-- close .event-featured-blog-pro -->
		<?php else: ?>
			
			<?php if(get_post_meta($post->ID, 'pro_video_post', true)): ?>
				<div class="event-featured-blog-pro grid2column-progression"><?php echo apply_filters('the_content', get_post_meta($post->ID, 'pro_video_post', true)); ?></div>
			<?php else: ?>
				<?php if(has_post_thumbnail()): ?>
					<div class="event-featured-blog-pro grid2column-progression"><?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large'); ?><a href="<?php echo esc_attr($image[0]);?>" data-rel="prettyPhoto"><?php the_post_thumbnail('progression-event'); ?></a></div>
				<?php endif; ?>
			<?php endif; ?>
	
		<?php endif; ?>

		<div class="event-entry-content-pro grid2column-progression lastcolumn-progression">			
			<div class="event-summary-post-pro">
				<?php do_action ('em_after_single_event_title');?>				
				<?php the_content(); ?>				
			</div><!-- close .summary-post-pro -->
			
			<?php wp_link_pages( array(
					'before' => '<div class="page-nav-pro">' . esc_html__( 'Pages:', 'progression' ),
					'after'  => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
			?>									
		</div><!-- .event-entry-content-pro -->	
		<div class="clearfix-pro"></div>
	</div><!-- close .post-container-pro -->
</article><!-- #post-## -->
