<?php
/**
 * @package progression
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-container-pro">
		
		<?php if(has_post_thumbnail()): ?>
			<div class="featured-blog-pro"><?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php else: ?><a href="<?php the_permalink(); ?>"><?php endif; ?><?php the_post_thumbnail('progression-blog'); ?></a></div>
		<?php else: ?>
			<?php if(get_post_meta($post->ID, 'pro_gallery', true) ): ?>
				<div class="featured-blog-pro">
					<div class="flexslider gallery-progression">
			      		<ul class="slides">
							<?php $gallery_pro = get_post_meta( get_the_id(), 'pro_gallery', false ); foreach ( $gallery_pro as $single_gallery_img ) ?>
							<?php if($gallery_pro):  foreach($gallery_pro as $single_gallery_img): ?>
								<?php $image = wp_get_attachment_image_src($single_gallery_img, 'progression-blog'); ?>				
								<li>
									<?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php else: ?><a href="<?php the_permalink(); ?>"><?php endif; ?><img src="<?php echo esc_attr($image[0]);?>" alt="Gallery Image"></a>
								</li>
							<?php endforeach; endif; ?>
						</ul>
					</div><!-- close .flexslider -->
				</div><!-- close .featured-blog-pro -->
			<?php else: ?>
				<?php if(get_post_meta($post->ID, 'pro_video_post', true)): ?>
					<div class="featured-blog-pro"><?php echo apply_filters('the_content', get_post_meta($post->ID, 'pro_video_post', true)); ?></div>
				<?php endif; ?>
			<?php endif; ?>
		
		<?php endif; ?>

		<div class="entry-content-pro">
			
			<div class="content-pro-title">
				<h1 class="blog-title-pro"><?php if(get_post_meta($post->ID, 'pro_external_link', true)): ?><a href="<?php echo esc_url( get_post_meta($post->ID, 'pro_external_link', true) );?>"><?php else: ?><a href="<?php the_permalink(); ?>"><?php endif; ?><?php the_title(); ?></a></h1>
				
				<div class="summary-post-pro"><?php the_content( sprintf( wp_kses( __( 'Continue Reading', 'progression' ), array( 'span' => array( 'class' => array() ) ) ), the_title( '<span class="screen-reader-text">"', '"</span>', false ) ) ); ?></div><!-- close .summary-post-pro -->
			</div>	
			
			<?php if ( 'post' == get_post_type() ) : ?>
				<div class="post-meta-pro">
					<?php progression_posted_on(); ?><br><span class="author-meta-pro"><?php _e( 'By', 'progression' ); ?> <?php the_author_posts_link(); ?></span><br><span class="cat-meta-pro"><?php the_category('<span>, </span>'); ?></span><br><?php comments_popup_link( '' . __( 'No Comments', 'progression' ) . '', _x( '1 Comment', 'comments number', 'progression' ), _x( '% Comments', 'comments number', 'progression' ) ); ?>
				</div>
			<?php endif; ?>
				
			
			
			<?php wp_link_pages( array(
					'before' => '<div class="page-nav-pro">' . esc_html__( 'Pages:', 'progression' ),
					'after'  => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
				) );
			?>
						
		</div><!-- .entry-content-pro -->
		
		<?php if ( is_sticky() && is_home() && ! is_paged() ) { printf( '<div class="sticky-post-pro">%s</div>', __( 'Featured', 'progression' ) ); } ?>
		<div class="clearfix-pro"></div>
	</div><!-- close .post-container-pro -->
</article><!-- #post-## -->