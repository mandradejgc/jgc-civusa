<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package progression
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<div class="page-content-pro">
			<?php the_content(); ?>
			<?php wp_link_pages( array(
					'before' => '<div class="page-nav-pro">' . esc_html__( 'Pages:', 'progression' ),
					'after'  => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );
			?>
		</div><!-- .page-content-pro -->
	
	</article><!-- #post-## -->
	<?php if (get_theme_mod( 'pro_global_comments') == "comment-off-pro") : ?><?php else: ?><?php if ( comments_open() || get_comments_number() ) : comments_template(); endif; ?><?php endif; ?>