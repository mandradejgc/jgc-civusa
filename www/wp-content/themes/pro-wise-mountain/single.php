<?php
/**
 * The template for displaying all single posts.
 *
 * @package progression
 */

get_header(); ?>

	<div id="page-header-pro"><div class="width-container-pro"><h1 class="entry-title-pro"><?php $page_for_posts = get_option('page_for_posts'); ?><?php echo get_the_title($page_for_posts); ?></h1></div></div><!-- #page-header-pro -->
	</header><!-- #masthead-pro -->

	<div id="content-pro" class="site-content">
		<div class="width-container-pro">
			
			<div id="main-container-pro">
				<?php while ( have_posts() ) : the_post(); ?>
					
					<?php get_template_part( 'template-parts/content', 'single' ); ?>
					
					<?php progression_content_nav(); ?>
				
					<?php if ( comments_open() || get_comments_number() ) : comments_template(); endif; ?>
					
				<?php endwhile; // end of the loop. ?>
			</div><!-- close #main-container-pro -->
			
			<?php get_sidebar(); ?>
			
		<div class="clearfix-pro"></div>
		</div><!-- close .width-container-pro -->
	</div><!-- #content-pro -->
<?php get_footer(); ?>