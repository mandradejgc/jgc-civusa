<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package progression
 * @since progression 1.0
 */
?>

<div class="clearfix-pro"></div>

	<footer id="site-footer">
		
		<?php if ( is_active_sidebar( 'Footer Widgets' ) ) { ?>
		<div id="widget-area-pro">
			<div class="width-container-pro <?php echo get_theme_mod('pro_widget_count', 'footer-1-pro'); ?>">
				<?php dynamic_sidebar( 'Footer Widgets' ); ?>
				<div class="clearfix-pro"></div>
			</div><!-- close .width-container-pro -->
		</div><!-- close #widget-area-pro -->
		<?php } ?>
		
		
		<div id="copyright-pro">
			<div class="width-container-pro <?php if (has_nav_menu( 'footer' )) : ?><?php else: ?> copyright-full-width<?php endif; ?>">
				<div id="copyright-text-pro"><?php echo esc_attr(get_theme_mod( 'footer_copyright_pro', '2015 All Rights Reserved. Developed by ProgressionStudios' )); ?></div>
			</div><!-- close .width-container-pro -->
			<div class="clearfix-pro"></div>
		</div><!-- close #copyright-pro -->
		
		<a href="#0" id="pro-scroll-top"><?php esc_html_e( 'Scroll to top', 'progression' ); ?></a>
		
	</footer>
	<?php if (get_theme_mod( 'pro_site_layout_wide' ) == 'boxed-pro') : ?></div><!-- close #boxed-layout-pro --><?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>