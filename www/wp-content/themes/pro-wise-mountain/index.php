<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package progression
 */

get_header(); ?>

	<?php if( get_option( 'page_for_posts' ) ) : $cover_page = get_page( get_option( 'page_for_posts' ) ); ?><?php if(get_post_meta($cover_page->ID, 'progression_slider', true)): ?><div id="pro-home-slider"><?php echo apply_filters('the_content', get_post_meta($cover_page->ID, 'progression_slider', true)); ?></div><?php else: ?><div id="page-header-pro"><div class="width-container-pro"><h1 class="entry-title-pro"><?php $page_for_posts = get_option('page_for_posts'); ?><?php echo get_the_title($page_for_posts); ?></h1></div></div><!-- #page-header-pro --><?php endif; ?><?php else: ?><div id="page-header-pro"><div class="width-container-pro"><h1 class="entry-title-pro"><?php _e( 'Latest News', 'progression' ); ?></h1></div></div><?php endif; ?>
	</header><!-- #masthead-pro -->

	<div id="content-pro" class="site-content">
		<div class="width-container-pro">
			
			<div id="main-container-pro">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
				<?php endwhile; ?>
				<?php show_pagination_links_pro( ); ?>
			<?php else : ?>
				<?php get_template_part( 'template-parts/content', 'none' ); ?>
			<?php endif; ?>
			</div><!-- close #main-container-pro -->
			
			<?php get_sidebar(); ?>
			
		<div class="clearfix-pro"></div>
		</div><!-- close .width-container-pro -->
	</div><!-- #content-pro -->
<?php get_footer(); ?>