<?php

/**
 *
 * @package progression
 * @since progression 1.0
 */

get_header(); ?>
	
		<?php if(is_page() && get_post_meta($post->ID, 'progression_slider', true) ): ?><?php else: ?><div id="page-header-pro"><div class="width-container-pro"><?php the_title( '<h1 class="entry-title-pro">', '</h1>' ); ?></div></div><!-- #page-header-pro --><?php endif; ?>
	</header><!-- #masthead-pro -->
	
<div class="clearfix-pro"></div>
	<div id="content-pro" >
		<div class="width-container-pro">
			
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
			<?php endwhile; ?>
			
			

		
		</div><!-- close .width-container-pro -->
	</div><!-- #content-pro -->
<?php get_footer(); ?>