<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?><div id="page-header-pro"><div class="width-container-pro"><h1 class="entry-title-pro"><?php woocommerce_page_title(); ?></h1></div></div><!-- #page-header-pro --><?php endif; ?>
	</header><!-- #masthead-pro -->
	
	<div id="content-pro" >
		<div class="width-container-pro">
			
			<div id="main-container-pro">
				<?php do_action( 'woocommerce_before_main_content' ); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'single-product' ); ?>

				<?php endwhile; // end of the loop. ?>

				<?php do_action( 'woocommerce_after_main_content' ); ?>
			</div><!-- close #main-container-pro -->
				
			<?php do_action( 'woocommerce_sidebar' ); ?>

		<div class="clearfix-pro"></div>
		</div><!-- close .width-container-pro -->
	</div><!-- #content-pro -->
<?php get_footer( 'shop' ); ?>
