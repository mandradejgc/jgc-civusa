<?php
/*
Plugin Name: Pro: Wise Mountain Custom Post Types
Plugin URI: http://progressionstudios.com
Description: This plugin registers the Wines custom post type for the Wise-Mountain WordPress Theme
Version: 1.0
Author: Progression Studios
Author URI: http://progressionstudios.com/
Author Email: contact@progressionstudios.com
License: GNU General Public License v3.0
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Registering Custom Post Types
 */
/*** Registering Custom Post Type ***/
add_action('init', 'progression_portfolio_init');
function progression_portfolio_init() {	
	
	register_post_type(
		'wine',
		array(
			'labels' => array(
				'name' => __( 'Wines', 'progression' ),
				'singular_name' => __( 'Wine', 'progression' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'wine-type'),
			'supports' => array('title', 'excerpt', 'editor', 'thumbnail'),
			'can_export' => true,
		)
	);
	
	register_taxonomy('wine_category', 'wine', array('hierarchical' => true, 'label' => __( 'Wine Categories', 'progression' ), 'query_var' => true, 'rewrite' => array('slug' => 'wine-category'),));

}




?>