<?php

/**
 * Default Templates
 */

add_action( 'vc_load_default_templates_action','my_custom_template_for_vc' ); // Hook in
function my_custom_template_for_vc() {
    $data               = array(); // Create new array
    $data['name']       = __( ' Highlight 1', 'progression' ); // Assign name for your custom template
    $data['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/highlight_1_template_thumb.png', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px
    $data['content']    = <<<CONTENT
		[vc_row css=".vc_custom_1436875100963{padding-right: 10% !important;padding-bottom: 85px !important;padding-left: 10% !important;background-image: url() !important;}"][vc_column][vc_row_inner][vc_column_inner][vc_column_text]
		<h2 style="text-align: center;">Experience The Wise Mountain</h2>
		[/vc_column_text][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.[/vc_column_text][/vc_column_inner][/vc_row_inner][pro_btn pro_btn_text="Request a tasting at our cellar" btn_style="dark-style-pro" btn_align="center-pro" link="url:%23!||"][/vc_column][/vc_row]
CONTENT;
  
    vc_add_default_templates( $data );
}


add_action( 'vc_load_default_templates_action','secondmy_custom_template_for_vc' ); // Hook in
function secondmy_custom_template_for_vc() {
    $data               = array(); // Create new array
    $data['name']       = __( ' Highlight 2', 'progression' ); // Assign name for your custom template
    $data['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/highlight_2_template_thumb.png', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px
    $data['content']    = <<<CONTENT
		[vc_row full_width="stretch_row" css=".vc_custom_1438019578151{margin-top: 100px !important;margin-bottom: 100px !important;padding-top: 100px !important;padding-bottom: 100px !important;background-color: #deded4 !important;}"][vc_column][vc_row_inner][vc_column_inner width="1/2"][vc_single_image img_size="full"][/vc_column_inner][vc_column_inner width="1/2" css=".vc_custom_1438618478124{padding-top: 30px !important;padding-right: 30px !important;padding-left: 30px !important;}"][vc_column_text]
		<h2 style="text-align: left;"><span style="color: #505045;">Location</span></h2>
		[/vc_column_text][vc_column_text]
		<p style="text-align: left;"><span style="color: #525245;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</span></p>
		[/vc_column_text][pro_btn pro_btn_text="Explore our estate" btn_style="modern-style-pro" btn_align="left-pro" link="url:%23!||"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]
CONTENT;
  
    vc_add_default_templates( $data );
}





add_action( 'vc_load_default_templates_action','thirdy_custom_template_for_vc' ); // Hook in
function thirdy_custom_template_for_vc() {
    $data               = array(); // Create new array
    $data['name']       = __( ' Highlight 3', 'progression' ); // Assign name for your custom template
    $data['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/highlight_3_template_thumb.png', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px
    $data['content']    = <<<CONTENT
		[vc_row full_width="stretch_row_content_no_spaces" css=".vc_custom_1438631529462{margin-bottom: -60px !important;background-color: #141414 !important;}"][vc_column width="1/2" css=".vc_custom_1438631330977{padding-top: 110px !important;padding-right: 30px !important;padding-bottom: 110px !important;padding-left: 30px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="base-footer-pro"][vc_custom_heading text="FROM FARM
		TO TABLE" font_container="tag:div|font_size:52px|text_align:center|color:%23ffffff|line_height:1.1" use_theme_fonts="yes" link="||"][/vc_column][vc_column width="1/2" css=".vc_custom_1438631336030{padding-top: 110px !important;padding-right: 30px !important;padding-bottom: 110px !important;padding-left: 30px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="base-footer-pro"][vc_custom_heading text="2015 FESTIVAL
		WINNERS" font_container="tag:div|font_size:52px|text_align:center|color:%23ffffff|line_height:1.1" use_theme_fonts="yes" link="||"][/vc_column][/vc_row]
CONTENT;
  
    vc_add_default_templates( $data );
}



add_action( 'vc_load_default_templates_action','fourthy_custom_template_for_vc' ); // Hook in
function fourthy_custom_template_for_vc() {
    $data               = array(); // Create new array
    $data['name']       = __( ' Homepage Template', 'progression' ); // Assign name for your custom template
    $data['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/homepage_template_thumb.jpg', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px
    $data['content']    = <<<CONTENT
		[vc_row css=".vc_custom_1438019568771{margin-bottom: 100px !important;padding-right: 10% !important;padding-left: 10% !important;background-image: url(http://localhost/wise-mountain/wp-content/uploads/2015/07/transparent-logo.png?id=17) !important;}"][vc_column][vc_row_inner][vc_column_inner][vc_column_text]
		<h2 style="text-align: center;">Experience The Wise Mountain</h2>
		[/vc_column_text][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.[/vc_column_text][/vc_column_inner][/vc_row_inner][pro_btn pro_btn_text="Request a tasting at our cellar" btn_style="dark-style-pro" btn_align="center-pro" link="url:%23!||"][/vc_column][/vc_row][vc_row full_width="stretch_row" css=".vc_custom_1438019578151{margin-top: 100px !important;margin-bottom: 100px !important;padding-top: 100px !important;padding-bottom: 100px !important;background-color: #deded4 !important;}"][vc_column][vc_row_inner][vc_column_inner width="1/2"][vc_single_image img_size="full"][/vc_column_inner][vc_column_inner width="1/2" css=".vc_custom_1438618478124{padding-top: 30px !important;padding-right: 30px !important;padding-left: 30px !important;}"][vc_column_text]
		<h2 style="text-align: left;"><span style="color: #505045;">Location</span></h2>
		[/vc_column_text][vc_column_text]
		<p style="text-align: left;"><span style="color: #525245;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla</span></p>

		[/vc_column_text][pro_btn pro_btn_text="Explore our estate" btn_style="modern-style-pro" btn_align="left-pro" link="url:%23!||"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row css=".vc_custom_1438019585924{margin-top: 100px !important;margin-bottom: 100px !important;padding-right: 10% !important;padding-left: 10% !important;}"][vc_column][vc_column_text]
		<h2 style="text-align: center;">Learn about the Process</h2>
		[/vc_column_text][vc_column_text]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste[/vc_column_text][/vc_column][/vc_row][vc_row full_width="stretch_row_content_no_spaces" css=".vc_custom_1438618555737{margin-top: 100px !important;margin-right: 0px !important;margin-bottom: 100px !important;margin-left: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}" el_class="stretch-progression"][vc_column css=".vc_custom_1437322301673{padding-right: 0px !important;padding-left: 0px !important;}"][pro_media_grid pro_img_size="large" pro_media_columns="3" pro_lightbox_check="true"][/vc_column][/vc_row][vc_row css=".vc_custom_1438019602930{margin-top: 100px !important;margin-bottom: 0px !important;}"][vc_column][pro_wine heading_pro="Our Selection" wine_cat_pro="Chardonnay,Merlot,Riesling" wine_columns_pro="3"][/vc_column][/vc_row][vc_row full_width="stretch_row_content_no_spaces" css=".vc_custom_1438631529462{margin-bottom: -60px !important;background-color: #141414 !important;}"][vc_column width="1/2" css=".vc_custom_1438631330977{padding-top: 110px !important;padding-right: 30px !important;padding-bottom: 110px !important;padding-left: 30px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="base-footer-pro"][vc_custom_heading text="FROM FARM
		TO TABLE" font_container="tag:div|font_size:52px|text_align:center|color:%23ffffff|line_height:1.1" use_theme_fonts="yes" link="||"][/vc_column][vc_column width="1/2" css=".vc_custom_1438631336030{padding-top: 110px !important;padding-right: 30px !important;padding-bottom: 110px !important;padding-left: 30px !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}" el_class="base-footer-pro"][vc_custom_heading text="2015 FESTIVAL
		WINNERS" font_container="tag:div|font_size:52px|text_align:center|color:%23ffffff|line_height:1.1" use_theme_fonts="yes" link="||"][/vc_column][/vc_row]
CONTENT;
  
    vc_add_default_templates( $data );
}