<?php

/**
 * Truck Location Shortcode
 */


// [pro_media_grid pro_media_attachments="" etc...]
add_shortcode( 'pro_media_grid', 'pro_media_grid_func' );
function pro_media_grid_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
	   'pro_media_attachments' => '',
	   'pro_media_columns' => '',
	   'pro_gap_images' => '',
	   'pro_lightbox_check' => '',
	   'pro_img_size' => 'thumbnail',
   ), $atts ) );
   
	$output_pro = '';
	
	ob_start();
	?>
		
		
		<div id="pro-media-grid">

			
			<?php if($pro_media_attachments) : ?>
				
				<?php $arr= explode(",",$pro_media_attachments);
				$count = 1; $count_2 = 1;
				 foreach($arr as $id):
					  if($count >= 1+$pro_media_columns) { $count = 1; }
					  ?>
					<?php $full_size = wp_get_attachment_image_src($id, 'large'); ?>
					<?php $img = wp_get_attachment_image_src($id, $pro_img_size); ?>
					<div class="pro-media-image pro-<?php echo esc_attr( $pro_media_columns ) ?>-column-media">
						<div class="pro-media-padding" <?php if($pro_gap_images) : ?>style="padding:<?php echo esc_attr( $pro_gap_images ) ?>;"<?php endif; ?>>
							<?php if($pro_lightbox_check) : ?><a href="<?php echo esc_attr($full_size[0]);?>" data-rel="prettyPhoto[gallery-pro]"><?php endif; ?><img src="<?php echo esc_attr($img[0]);?>" width="<?php echo esc_attr($img[1]);?>" height="<?php echo esc_attr($img[2]);?>" alt="gallery"><?php if($pro_lightbox_check) : ?></a><?php endif; ?>
						</div>
					</div>
					
					<?php if($count == $pro_media_columns): ?><div class="clearfix-pro"></div><?php endif; ?>
				<?php $count ++; $count_2++; endforeach; ?>
				
			
			<?php endif; ?>
			
			<div class="clearfix-pro"></div>
		</div>
		
		

    <?php
	
   	return $output_pro. ob_get_clean();
	
}


add_action( 'vc_before_init', 'pro_media_grid_integrateVC' );
function pro_media_grid_integrateVC() {
   vc_map( array(
      "name" => __( "Pro Media Grid", "progression" ),
	  "description" => __( "Image Grid", "progression" ),
      "base" => "pro_media_grid",
	  "weight" => 100,
      "class" => "",
      "category" => __( "Pro Elements", "progression"),
	  'icon' => 'vc-icon',

      "params" => array(
          array(
            "type" => "attach_images",
 			"class" => "",
             "heading" => __( "Image Gallery", "progression" ),
             "param_name" => "pro_media_attachments",
             "value" => __( "", "progression" ),
          ),
  		array(
  			'type' => 'dropdown',
  			'heading' => __( 'Image size', 'progression' ),
  			'param_name' => 'pro_img_size',
  			"value"       => array(
  			        'Thumbnail'   => 'thumbnail',
  			        'Medium'   => 'medium',
  			        'Large'	  => 'large',
  			        'Full Size'   => 'full',
  			),
  			"std"         => 'full',
  		),
          array(
            "type" => "dropdown",
 			"class" => "",
             "heading" => __( "Images per row", "progression" ),
             "param_name" => "pro_media_columns",
  			"value"       => array(
  			        '1'   => '1',
  			        '2'   => '2',
  			        '3'	  => '3',
  			        '4'   => '4',
					'5'   => '5',
					'6'   => '6',
  			),
  			"std"         => '0',
          ),
          array(
            "type" => "dropdown",
 			"class" => "",
             "heading" => __( "Gap between images", "progression" ),
             "param_name" => "pro_gap_images",
  			"value"       => array(
  			        'No gap'   => '0px',
  			        '1px'   => '1px',
  			        '2px'	  => '2px',
  			        '3px'   => '3px',
					'4px'   => '4px',
					'5px'   => '5px',
					'10px'   => '10px',
					'15px'   => '15px',
					'20px'   => '20px',
  			),
  			"std"         => '0px',
          ),
          array(
            "type" => "checkbox",
 			"class" => "",
             "heading" => __( "Open image in lightbox", "progression" ),
             "param_name" => "pro_lightbox_check",
             "value" => __( "0", "progression" ),
          ),
          
      )
   ) );
}