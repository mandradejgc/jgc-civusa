<?php

/**
 * Button Shortcode
 */

// [pro_btn pro_btn_text="Text on the button"]
add_shortcode( 'pro_btn', 'pro_btn_func' );
function pro_btn_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
      'pro_btn_text' => 'Text on the button',
	  'link' => '',
	  'btn_style' => '',
	  'btn_align' => '',
	  'btn_size' => 'normal-size-pro',
	  'btn_class' => '',
	  'pro_btn_icon' => '',
	  'icon_alignment' => '',
	  'icon_type' => '',
	  'icon_fontawesome' => '',
	  'icon_openiconic' => '',
	  'icon_typicons' => '',
	  'icon_entypo' => '',
	  'icon_linecons' => '',
   ), $atts ) );
  
   
	$link = ( $link == '||' ) ? '' : $link;
	$link = vc_build_link( $link );
	$use_link = false;
	if ( strlen( $link['url'] ) > 0 ) {
		$use_link = true;
		$a_href = $link['url'];
		$a_title = $link['title'];
		$a_target = strlen( $link['target'] ) > 0 ? $link['target'] : '_self';
	}
   
   $icon_wrapper = false;
   
	if ( 'true' === $pro_btn_icon ) {
		vc_icon_element_fonts_enqueue( $icon_type );

		if ( isset( ${"i_icon_" . $icon_type} ) ) {
			if ( 'pixelicons' === $icon_type ) {
				$icon_wrapper = true;
			}
			$iconClass = ${"i_icon_" . $icon_type};
		} else {
			
			if ( 'openiconic' === $icon_type ) {
				$iconClass = $icon_openiconic;
			}
			elseif ( 'typicons' === $icon_type ) {
				$iconClass = $icon_typicons;
			}
			elseif ( 'entypo' === $icon_type ) {
				$iconClass = $icon_entypo;
			}
			elseif ( 'linecons' === $icon_type ) {
				$iconClass = $icon_linecons;
			}
			 else {
			$iconClass = $icon_fontawesome;}
		}

		if ( $icon_wrapper ) {
			$icon_html = '<i class="vc_btn3-icon"><span class="vc_btn3-icon-inner ' . esc_attr( $iconClass ) . '"></span></i>';
		} else {
			$icon_html = '<i class="vc_btn3-icon ' . esc_attr( $iconClass ) . '"></i>';
		}


		if ( $icon_alignment == 'left-pro' ) {
			$pro_btn_text = $icon_html . ' ' . $pro_btn_text;
		} else {
			$pro_btn_text .= ' ' . $icon_html;
		}
	}
   
   
   	$output_pro = '';
	$output_pro .= '<div class="pro-btn-container ' . $btn_align . '">';
		$output_pro .= '<a ';
			if ( $a_href ) { $output_pro .= 'href="' . $a_href . '"'; }
			if ( $a_target ) { $output_pro .= ' target="' . $a_target . '"'; }
			if ( $a_title ) { $output_pro .= 'title="' . $a_title . '"'; }
		$output_pro .= ' class="pro-button-shortcode ' . $btn_style . ' ' . $btn_size . ' ' . $btn_class .'">';
			
			$output_pro .= $pro_btn_text;
			
		$output_pro .= '</a>';
	$output_pro .= '</div>';
  
   return $output_pro;
}


add_action( 'vc_before_init', 'pro_btn_integrateWithVC' );
function pro_btn_integrateWithVC() {
   vc_map( array(
      "name" => __( "Pro Button", "progression" ),
	  "description" => __( "Eye catching button", "progression" ),
      "base" => "pro_btn",
	  "weight" => 100,
      "class" => "",
      "category" => __( "Pro Elements", "progression"),
	  'icon' => 'vc-icon',

      "params" => array(
         array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __( "Text", "progression" ),
            "param_name" => "pro_btn_text",
            "value" => __( "Text on the button", "progression" ),
         ),
 		array(
 			'type' => 'vc_link',
 			'heading' => __( 'URL (Link)', 'progression' ),
 			'param_name' => 'link',
 			'description' => __( 'Add link to button.', 'progression' )
 		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Style', 'progression' ),
			'param_name' => 'btn_style',
			'value' => array(
				__( 'Default', 'progression' ) => "default-style-pro",
				__( 'Dark', 'progression' ) => 'dark-style-pro',
				__( 'Modern', 'progression' ) => 'modern-style-pro',
				__( 'Light', 'progression' ) => 'light-style-pro',
			),
			"std"         => 'default-style-pro',
			'description' => __( 'Select button display style.', 'progression' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Alignment', 'progression' ),
			'param_name' => 'btn_align',
			'value' => array(
				__( 'Inline', 'progression' ) => "inline-pro",
				__( 'Left', 'progression' ) => 'left-pro',
				__( 'Center', 'progression' ) => 'center-pro',
				__( 'Right', 'progression' ) => "right-pro"
			),
			'description' => __( 'Select button alignment.', 'progression' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Size', 'progression' ),
			'param_name' => 'btn_size',
			'value' => array(
				__( 'Small', 'progression' ) => "small-size-pro",
				__( 'Normal', 'progression' ) => 'normal-size-pro',
				__( 'Large', 'progression' ) => 'large-size-pro',
			),
			"std"         => 'normal-size-pro',
			'description' => __( 'Select button display size.', 'progression' )
		),
        array(
           "type" => "checkbox",
           "class" => "",
           "heading" => __( "Add icon?", "progression" ),
           "param_name" => "pro_btn_icon",
        ),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Icon Alignment', 'progression' ),
			'value' => array(
				__( 'Left', 'progression' ) => 'left-pro',
				__( 'Right', 'progression' ) => 'right-pro',
			),
			'dependency' => array(
				'element' => 'pro_btn_icon',
				'not_empty' => true,
			),
			'param_name' => 'icon_alignment',
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Icon library', 'progression' ),
			'value' => array(
				__( 'Font Awesome', 'progression' ) => 'fontawesome',
				__( 'Open Iconic', 'progression' ) => 'openiconic',
				__( 'Typicons', 'progression' ) => 'typicons',
				__( 'Entypo', 'progression' ) => 'entypo',
				__( 'Linecons', 'progression' ) => 'linecons',
			),
			'dependency' => array(
				'element' => 'pro_btn_icon',
				'not_empty' => true,
			),
			'param_name' => 'icon_type',
			'description' => __( 'Select icon library.', 'progression' ),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'progression' ),
			'param_name' => 'icon_fontawesome',
			'value' => 'fa fa-info-circle',
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'iconsPerPage' => 200, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'icon_type',
				'value' => 'fontawesome',
			),
			'description' => __( 'Select icon from library.', 'progression' ),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'progression' ),
			'param_name' => 'icon_openiconic',
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'type' => 'openiconic',
				'iconsPerPage' => 200, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'icon_type',
				'value' => 'openiconic',
			),
			'description' => __( 'Select icon from library.', 'progression' ),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'progression' ),
			'param_name' => 'icon_typicons',
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'type' => 'typicons',
				'iconsPerPage' => 200, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'icon_type',
				'value' => 'typicons',
			),
			'description' => __( 'Select icon from library.', 'progression' ),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'progression' ),
			'param_name' => 'icon_entypo',
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'type' => 'entypo',
				'iconsPerPage' => 300, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'icon_type',
				'value' => 'entypo',
			),
		),
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'progression' ),
			'param_name' => 'icon_linecons',
			'settings' => array(
				'emptyIcon' => false, // default true, display an "EMPTY" icon?
				'type' => 'linecons',
				'iconsPerPage' => 200, // default 100, how many icons per/page to display
			),
			'dependency' => array(
				'element' => 'icon_type',
				'value' => 'linecons',
			),
			'description' => __( 'Select icon from library.', 'progression' ),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'progression' ),
			'param_name' => 'btn_class',
			'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'progression' )
		)
      )
   ) );
}