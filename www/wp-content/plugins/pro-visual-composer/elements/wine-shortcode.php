<?php

/**
 * Wine Shortcode
 */


// [pro_wine heading_pro="" wine_description_pro="" etc...]
add_shortcode( 'pro_wine', 'pro_wine_func' );
function pro_wine_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
      'heading_pro' => '',
	  'wine_description_pro' => '',
	  'wine_cat_pro' => '',
	  'wine_columns_pro' => '3',
   ), $atts ) );
   
	$output_pro = '';
   	if ( $heading_pro ) {
   		$output_pro .= '<h2 class="wine-heading-pro"><span>' . $heading_pro .'</span></h2><div class="clearfix-pro"></div>';
   	}
   	if ( $wine_description_pro ) {
   		$output_pro .= '<div class="wine-description-pro">' . $wine_description_pro .'</div><div class="clearfix-pro"></div>';
   	}
	
	
	$output_pro .= '<div class="wine-container-pro"><div class="width-container-pro">';
	
	
	$postIds = $wine_cat_pro; // get custom field value
    $arrayIds = explode(',', $postIds); // explode value into an array of ids
    if(count($arrayIds) <= 1) // if array contains one element or less, there's spaces after comma's, or you only entered one id
    {
        if( strpos($arrayIds[0], ',') !== false )// if the first array value has commas, there were spaces after ids entered
        {
            $arrayIds = array(); // reset array
            $arrayIds = explode(', ', $postIds); // explode ids with space after comma's
        }
    }
	
	
	if ( $wine_cat_pro ) {
	   	$carousel_query = new WP_Query(
	   		array(
	   	        'post_type' => 'wine',
	   	        'posts_per_page' => 99,
			
				'tax_query' => array(
				        // Note: tax_query expects an array of arrays!
				        array(
				            'taxonomy' => 'wine_category', // my guess
				            'field'    => 'slug',
				            'terms'    => $arrayIds
				        )
				 ),
			 
	   		)
	    );
	} else {
	   	$carousel_query = new WP_Query(
	   		array(
	   	        'post_type' => 'wine',
	   	        'posts_per_page' => 99,

	   		)
	    );
	}
	
	$count = 1; $count_2 = 1;
	
	ob_start();
	?>
	
		<?php 
		while ($carousel_query->have_posts()) : $carousel_query->the_post();
		if($count >= 1+$wine_columns_pro) { $count = 1; }
		?>
   			<div class="grid<?php echo esc_attr( $wine_columns_pro ) ?>column-progression<?php if($count == $wine_columns_pro ): echo ' lastcolumn-progression'; endif; ?>">
				<?php get_template_part( 'template-parts/content', 'wine' ); ?>
			</div>
			<?php if($count == $wine_columns_pro): ?><div class="clearfix-pro"></div><?php endif; ?>
		<?php $count ++; $count_2++;
	endwhile; 
	wp_reset_query(); ?>
		
   <?php
	
   	return '</div><!-- close .width-container-pro --></div><!-- close .wine-container-pro --><div class="clearfix-pro"></div>' . $output_pro. ob_get_clean();
	
}


add_action( 'vc_before_init', 'pro_wine_integrateVC' );
function pro_wine_integrateVC() {
   vc_map( array(
      "name" => __( "Wine Posts", "progression" ),
	  "description" => __( "List your Wine items", "progression" ),
      "base" => "pro_wine",
	  "weight" => 100,
      "class" => "",
      "category" => __( "Pro Elements", "progression"),
	  'icon' => 'vc-icon',

      "params" => array(
         array(
            "type" => "textfield",
			"holder" => "div",
			"class" => "",
            "heading" => __( "Heading", "progression" ),
            "param_name" => "heading_pro",
            "value" => __( "", "progression" ),
         ),
         array(
            "type" => "textarea",
			"holder" => "div",
			"class" => "",
            "heading" => __( "Description", "progression" ),
            "param_name" => "wine_description_pro",
            "value" => __( "", "progression" ),
         ),
         array(
            "type" => "textfield",
			"holder" => "div",
			"class" => "",
            "heading" => __( "Narrow Wine Categories", "progression" ),
            "param_name" => "wine_cat_pro",
            "value" => __( "", "progression" ),
			"description" => __( "Enter Wine category slugs to display a specific category. Add-in multiple category slugs separated by a comma to use multiple categories. (Leave blank to pull in all wine posts).", "progression" ),
         ),
         array(
            "type" => "dropdown",
			"class" => "",
            "heading" => __( "Wine Columns", "progression" ),
            "param_name" => "wine_columns_pro",
			"value"       => array(
			        '1 Column'   => '1',
			        '2 Columns'   => '2',
			        '3 Columns'	  => '3',
			        '4 Columns'   => '4',
					'5 Columns'   => '5',
					'6 Columns'   => '6'
			),
			"std"         => '2',
         ),
      )
   ) );
}