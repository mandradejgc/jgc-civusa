<?php

/**
 * Event Shortcode
 */


// [pro_event heading_pro="" event_description_pro="" etc...]
add_shortcode( 'pro_event', 'pro_event_func' );
function pro_event_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
      'heading_pro' => '',
	  'event_description_pro' => '',
	  'event_cat_pro' => '',
	  'event_posts_pro' => '99',
   ), $atts ) );
   
	$output_pro = '';
   	if ( $heading_pro ) {
   		$output_pro .= '<h2 class="event-heading-pro"><div class="width-container-pro"><span>' . $heading_pro .'</span></div></h2><div class="clearfix-pro"></div>';
   	}
   	if ( $event_description_pro ) {
   		$output_pro .= '<div class="event-description-pro">' . $event_description_pro .'</div><div class="clearfix-pro"></div>';
   	}
	
	
	$output_pro .= '<div class="event-container-pro"><div class="width-container-pro">';
	
	
	$postIds = $event_cat_pro; // get custom field value
    $arrayIds = explode(',', $postIds); // explode value into an array of ids
    if(count($arrayIds) <= 1) // if array contains one element or less, there's spaces after comma's, or you only entered one id
    {
        if( strpos($arrayIds[0], ',') !== false )// if the first array value has commas, there were spaces after ids entered
        {
            $arrayIds = array(); // reset array
            $arrayIds = explode(', ', $postIds); // explode ids with space after comma's
        }
    }
	
	
	if ( $event_cat_pro ) {
	   	$carousel_query = new WP_Query(
	   		array(
	   	        'post_type' => 'event',
	   	        'posts_per_page' => $event_posts_pro,
			
				'tax_query' => array(
				        // Note: tax_query expects an array of arrays!
				        array(
				            'taxonomy' => 'event-category', // my guess
				            'field'    => 'slug',
				            'terms'    => $arrayIds
				        )
				 ),
			 
	   		)
	    );
	} else {
	   	$carousel_query = new WP_Query(
	   		array(
	   	        'post_type' => 'event',
	   	        'posts_per_page' => $event_posts_pro,

	   		)
	    );
	}
	
	$count = 1; $count_2 = 1;
	
	ob_start();
	?>
		<ul class="event-list-pro">
			<?php 
			while ($carousel_query->have_posts()) : $carousel_query->the_post();		
			?>
				<?php get_template_part( 'template-parts/content', 'event' );	?>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</ul>
		
   <?php
	
   	return '</div><!-- close .width-container-pro --></div><!-- close .event-container-pro --><div class="clearfix-pro"></div>' . $output_pro. ob_get_clean();
	
}


add_action( 'vc_before_init', 'pro_event_integrateVC' );
function pro_event_integrateVC() {
   vc_map( array(
      "name" => __( "Event Posts", "progression" ),
	  "description" => __( "List your Event items", "progression" ),
      "base" => "pro_event",
	  "weight" => 100,
      "class" => "",
      "category" => __( "Pro Elements", "progression"),
	  'icon' => 'vc-icon',

      "params" => array(
         array(
            "type" => "textfield",
			"holder" => "div",
			"class" => "",
            "heading" => __( "Heading", "progression" ),
            "param_name" => "heading_pro",
            "value" => __( "", "progression" ),
         ),
         array(
            "type" => "textarea",
			"holder" => "div",
			"class" => "",
            "heading" => __( "Description", "progression" ),
            "param_name" => "event_description_pro",
            "value" => __( "", "progression" ),
         ),
         array(
            "type" => "textfield",
			"holder" => "div",
			"class" => "",
            "heading" => __( "Narrow Event Categories", "progression" ),
            "param_name" => "event_cat_pro",
            "value" => __( "", "progression" ),
			"description" => __( "Enter Event category slugs to display a specific category. Add-in multiple category slugs separated by a comma to use multiple categories. (Leave blank to pull in all event posts).", "progression" ),
         ),
         array(
            "type" => "textfield",
			"holder" => "div",
			"class" => "",
            "heading" => __( "Number of Event Posts", "progression" ),
            "param_name" => "event_posts_pro",
			"value" => __( "", "progression" ),
			"description" => __( "Enter the number of event posts you wish to display on the page.", "progression" ),
         ),
      )
   ) );
}