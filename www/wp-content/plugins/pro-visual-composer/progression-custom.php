<?php
/*
Plugin Name: Progression VC Elements
Plugin URI: http://progressionstudios.com
Description: This plugin registers the beer custom post type for Progression Studios
Version: 1.0
Author: Progression Studios
Author URI: http://progressionstudios.com/
Author Email: contact@progressionstudios.com
License: GNU General Public License v3.0
*/


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}




// CSS Styles in Admin
function pro_vc_wp_admin_style() {
    wp_register_style( 'pro-vc-styles',  plugin_dir_url( __FILE__ ) . 'assets/style.css' );
    wp_enqueue_style( 'pro-vc-styles' );
}
add_action( 'admin_enqueue_scripts', 'pro_vc_wp_admin_style' );



/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
add_action( 'vc_before_init', 'progression' );
function progression() { vc_set_as_theme(); }


//Default Parameters if ( ! function_exists( 'js_composer' ) ) require_once( plugin_dir_path( __FILE__ ) . 'elements/default-parameters.php' );


// Element Shortcodes
if ( ! function_exists( 'js_composer' ) ) require_once( plugin_dir_path( __FILE__ ) . 'elements/wine-shortcode.php' );
if ( ! function_exists( 'js_composer' ) ) require_once( plugin_dir_path( __FILE__ ) . 'elements/event-shortcode.php' );
if ( ! function_exists( 'js_composer' ) ) require_once( plugin_dir_path( __FILE__ ) . 'elements/map-shortcode.php' );
if ( ! function_exists( 'js_composer' ) ) require_once( plugin_dir_path( __FILE__ ) . 'elements/media-grid.php' );
if ( ! function_exists( 'js_composer' ) ) require_once( plugin_dir_path( __FILE__ ) . 'elements/button-shortcode.php' );


//Default Templates
if ( ! function_exists( 'js_composer' ) ) require_once( plugin_dir_path( __FILE__ ) . 'templates/custom-templates.php' );


?>