<?php
/**
 * Managed by i2TIC Managment System!!!! Your changes will be overwrite. 
 * To make permanent changes talk to soporte@i2tic.com
 * 
 * The base configuration for WordPress
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if( $_SERVER['HTTPS'] )
{
	define('WP_SITEURL', 'https://' . $_SERVER['HTTP_HOST']);
	define('WP_HOME',  'https://' . $_SERVER['HTTP_HOST']);
}
else
{
	define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST']);
	define('WP_HOME',  'http://' . $_SERVER['HTTP_HOST']);
}

/** DB  CONF **/
define('DB_NAME', '{{pillar.get('wp').get('DB_NAME')}}');

/** MySQL database username */
define('DB_USER', '{{pillar.get('wp').get('DB_USER')}}');

/** MySQL database password */
define('DB_PASSWORD', '{{pillar.get('wp').get('DB_PASSWORD')}}');

/** MySQL hostname */
define('DB_HOST', '{{pillar.get('wp').get('DB_HOST')}}');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', '{{pillar.get('wp').get('DB_CHARSET', 'utf8')}}');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '{{pillar.get('wp').get('DB_COLLATE', '')}}');

/** WordPress Database Table prefix.
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = '{{pillar.get('wp').get('table_prefix', '')}}';


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{{pillar.get('wp').get('AUTH_KEY')}}');
define('SECURE_AUTH_KEY',  '{{pillar.get('wp').get('SECURE_AUTH_KEY')}}');
define('LOGGED_IN_KEY',    '{{pillar.get('wp').get('LOGGED_IN_KEY')}}');
define('NONCE_KEY',        '{{pillar.get('wp').get('NONCE_KEY')}}');
define('AUTH_SALT',        '{{pillar.get('wp').get('AUTH_SALT')}}');
define('SECURE_AUTH_SALT', '{{pillar.get('wp').get('SECURE_AUTH_SALT')}}');
define('LOGGED_IN_SALT',   '{{pillar.get('wp').get('LOGGED_IN_SALT')}}');
define('NONCE_SALT',       '{{pillar.get('wp').get('NONCE_SALT')}}');
/**#@-*/


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', {{pillar.get('wp').get('WP_DEBUG', 'false')}});

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');
?>

